function [Rout_tp,Rout] = outputSet(obj,options,Rstart,Rti)
% outputSet - calculates output set based on output equation given by
%    y = Cx + Du + k + v and sets for x (R) and u (options.U + options.uTrans)
%
% Syntax:  
%    [Rout_tp,Rout] = outputSet(C,D,k,R,options)
%
% Inputs:
%    obj     - linearSys object
%    options - options for the computation of reachable sets
%    Rstart  - reachable set of time point [i]
%    Rti     - reachable set of time interval [i,i+1]
%
% Outputs:
%    Rout_tp - output set of time point [i]
%    Rout    - output set of time interval [i,i+1]
%
% Example:
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: -
% 
% Author:       Mark Wetzlinger
% Written:      12-Aug-2019
% Last update:  20-Aug-2019
%               16-November-2021 (MW, add sensor noise V)
%               19-November-2021 (MW, shift index of time-point solution)
% Last revision:---

%------------- BEGIN CODE --------------

% output equation is not provided or y = x
if isempty(obj.C) || ...
        ( isscalar(obj.C) && obj.C == 1 && ~any(any(obj.D)) && ~any(obj.k) && ...
        ~any(center(options.V)) && isempty(generators(options.V)) )
    Rout_tp = Rstart; Rout = Rti;
    return;
end

isD = false;
if any(any(obj.D))
    isD = true;
    U = options.U + options.uTrans;
end


if ~isfield(options,'saveOrder')
    
    if isD
        Rout_tp = obj.C*Rstart + obj.D * U + obj.k + options.V;
        Rout = obj.C*Rti + obj.D * U + obj.k + options.V;
    else
        Rout_tp = obj.C*Rstart + obj.k + options.V;
        Rout = obj.C*Rti + obj.k + options.V;
    end

else

    % reduction by saveOrder
    if isD
        Rout_tp = reduce(zonotope(obj.C*Rstart) + obj.D * U + obj.k + options.V,...
            options.reductionTechnique,options.saveOrder);
        Rout = reduce(zonotope(obj.C*Rti) + obj.D * U + obj.k + options.V,...
            options.reductionTechnique,options.saveOrder);
    else
        Rout_tp = reduce(zonotope(obj.C*Rstart) + obj.k + options.V,...
            options.reductionTechnique,options.saveOrder);
        Rout = reduce(zonotope(obj.C*Rti) + obj.k + options.V,...
            options.reductionTechnique,options.saveOrder);
    end

end

end



%------------- END OF CODE --------------

