function han = plot(pZ,varargin)
% plot - Plots 2-dimensional over-approximation of a polynomial zonotope
%
% Syntax:  
%    han = plot(pZ)
%    han = plot(pZ,dim,color)
%    han = plot(pZ,dim,color,'Splits',splits)
%
% Inputs:
%    pZ - polyZonotope object
%    dim - dimensions that should be projected
%    color - color of the plotted set('r','b', etc.)
%    splits - number of splits for refinement (optional)
%
% Outputs:
%    han - handle for the resulting graphic object
%
% Example: 
%    pZ = polyZonotope([0;0],[2 0 1;0 2 1],[0;0],[1 0 3;0 1 1]);
%     
%    figure
%    hold on
%    plotRandPoint(pZ,[1,2],100000,'.r');
%    plot(pZ,[1,2],'b','Splits',3);
%
%    figure
%    hold on
%    plotRandPoint(pZ,[1,2],100000,'.r');
%    plot(pZ,[1,2],'b','Splits',10);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plotFilled, plotRandPoint

% Author:       Niklas Kochdumper
% Written:      29-March-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default values for the optional input arguments
dimensions = [1,2];
color = 'b';
splits = 8;

% parse input arguments
if nargin > 1 && ~isempty(varargin{1})
   dimensions = varargin{1}; 
end
if nargin > 2 && ~isempty(varargin{2})
   color = varargin{2};
end

type = [];
if nargin > 3
    type = varargin(3:end);
    for i = 1:2:length(type)-1
       if ischar(type{i}) && strcmp(type{i},'Splits')
           splits = type{i+1};
           type{i} = [];
           type{i+1} = [];
       end
    end
    type = type(~cellfun('isempty',type));
end

% delete all zero-generators
pZ = deleteZeros(pZ);

% split the polynomial zonotope multiple times to obtain a better 
% over-approximation of the real shape
pZsplit{1} = project(pZ,dimensions);

for i=1:splits
    pZnew = [];
    for j=1:length(pZsplit)
        res = splitLongestGen(pZsplit{j});
        pZnew{end+1} = res{1};
        pZnew{end+1} = res{2};
    end
    pZsplit = pZnew;
end

% over-approximate all splitted sets with zonotopes, convert them to 2D
% polytopes (Matlab built-in) and compute the union
warOrig = warning;
warning('off','all');

for i = 1:length(pZsplit)
   
    % zonotope over-approximation
    zono = zonotope(pZsplit{i});
    
    % calculate vertices of zonotope
    vert = vertices(zono);
    
    % transform to 2D polytope
    pgon = polyshape(vert(1,:),vert(2,:));
    
    % calculate union with previous sets
    if i == 1
       polyAll = pgon; 
    else
       polyAll = union(polyAll,pgon); 
    end  
end

warning(warOrig);


% plot the polygon
if isempty(type)
    han = plot(polyAll,'FaceAlpha',1,'FaceColor','none','EdgeColor',color);
else
    han = plot(polyAll,'FaceAlpha',1,'FaceColor', ...
               'none','EdgeColor',color,type{:});
end

%------------- END OF CODE --------------