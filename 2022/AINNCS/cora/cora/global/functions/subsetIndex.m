function index = subsetIndex(v,vs)
% subsetIndex - Stores entries of a matrix into a vector
%
% Syntax:  
%    index = subsetIndex(v,vs)
%
% Inputs:
%    v  - vector
%    vs - vector (each component must occur in v)
%
% Outputs:
%    index - index such that v(index) = vs
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Victor Gassmann
% Written:      25-February-2022 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

if length(v)~=numel(v) || length(vs) ~=numel(vs)
    error('Input arguments need to be vectors');
end
if ~all(ismember(vs,v))
    error('At least one component of "v_sub" not in "v"!');
end
if ~isa(v,'double') || ~isa(vs,'double')
    error('Input arguments must be double');
end

[r,ii_v,~] = intersect(v,vs,'stable');
% since vs is subset of v, r and vs are (up to permutations)
% the same => compute indices to go from r to vs, i.e. find ii_r such that
% r(ii_r) = vs
% the first argument is identical to vs (thanks to 'stable')
[~,~,ii_r] = intersect(vs,r,'stable');
index = ii_v(ii_r);

%------------- END OF CODE --------------
