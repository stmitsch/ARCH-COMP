\documentclass[a4paper]{easychair}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[scaled=0.8]{beramono}

\usepackage{todonotes}
\usepackage{cleveref}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{color}

\newcommand{\red}[1]{{\color{red}#1}}

\usepackage{newunicodechar}
\newunicodechar{□}{\Box}
\newunicodechar{◇}{\Diamond}
\newunicodechar{○}{\circ}

\title{ARCH-COMP 2020 Category Report: Falsification%
       \footnote{The falsification category was coordinated by the first author.
                 The remaining authors represent all participants and they are listed alphabetically.}}
\author{
    Gidon~Ernst\inst{1}
    \and
    Paolo~Arcaini \inst{2} \and
    Ismail Bennani \inst{7} \and
    Alexandre Donze \inst{3} \and \\
    Georgios~Fainekos \inst{4} \and
    Goran Frehse \inst{8} \and
    Logan~Mathesen \inst{4} \and
    Claudio~Menghi \inst{6} \and \\
    Giulia Pedrielli \inst{4} \and
    Marc Pouzet \inst 7 \and
    Shakiba~Yaghoubi \inst{4} \and
    Yoriyuki~Yamagata \inst{5} \and
    Zhenya~Zhang \inst{2}}

\institute{
	Ludwig-Maximilians-University (LMU), Munich, Germany \\
	\email{gidon.ernst@lmu.de}
\and
    National Institute of Informatics (NII), Tokyo, Japan \\
    \email{\{arcaini,zhangzy\}@nii.ac.jp}
\and
    Decyphir SAS, Moirans, France \\
    \email{alex@decyphir.com}
\and
    Arizona State University (ASU), Tempe, USA \\
    \email{\{fainekos,lmathese,gpedriel,syaghoub\}@asu.edu}
\and
    National Institute of Advanced Industrial Science and Technology (AIST), Osaka, Japan \\
    \email{yoriyuki.yamagata@aist.go.jp}
\and
    University of
Luxembourg, Luxembourg, Luxembourg \\
    \email{claudio.menghi@uni.lu}
\and
    \'Ecole Normale Supérieure (ENS), Paris, France \\
    \email{\{ismail.lahkim.bennani,marc.pouzet\}@ens.fr}
\and
    ENSTA Paris, Palaiseau, France \\
    \email{goran.frehse@ensta-paris.fr}
}


\titlerunning{ARCH-COMP 2020: Falsification}
\authorrunning{Ernst et al.}

\newcommand{\ARCH}{\url{https://cps-vo.org/group/ARCH/FriendlyCompetition}}
\newcommand{\gitlab}{\url{https://gitlab.com/goranf/ARCH-COMP}}

\newcommand{\STaLiRo}{S-TaLiRo\xspace}
\newcommand{\Breach}{Breach\xspace}
\newcommand{\FalStar}{\textsc{FalStar}\xspace}
\newcommand{\falsify}{falsify\xspace}
\newcommand{\ARIsTEO}{ARIsTEO\xspace}
\newcommand{\zlscheck}{\texttt{zlscheck}\xspace}


\newcommand{\FR}{\ensuremath{\mathit{FR}}}
\newcommand{\Savg}{\ensuremath{\overline{S}}}
\newcommand{\Smed}{\ensuremath{\widetilde{S}}}

\begin{document}
	\maketitle

	\begin{abstract}
      This report presents the results from the 2020 friendly
      competition in the ARCH workshop for the falsification of
      temporal logic specifications over Cyber-Physical Systems.
      We briefly describe the competition settings, which have been inherited from the previous year,
      give background on the participating teams and tools and discuss the selected benchmarks.
      The benchmarks are available on the ARCH website\footnote{\ARCH}, as well as in the competition's
      \texttt{gitlab} repository\footnote{\gitlab}.
      In comparison to 2019, we have two new participating tools with novel approaches,
      and the results show a clear improvement over previous performances on some benchmarks.
      % The main outcome of the 2019 competition is a common benchmark repository,
      % and an initial base-line for falsification, with results from multiple tools,
      % which will facilitate comparisons and tracking of the state-of-the-art
      % in falsification in the future.
	\end{abstract}

	\section{Introduction}

	The friendly competition of the ARCH workshop is running
    yearly since~2014. The goal is to compare the state-of-the-art of
    tools for testing and verification of hybrid systems.  The
    competition is organized in several categories, with different
    specifications (computing reachable regions, checking temporal
    properties) and varying dynamics in the system models (such as
    linear/non-linear and hybrid).

	In the falsification category, benchmarks typically consist of
    executable Matlab code or Simulink/Stateflow models, each
    associated with a set of requirements in temporal logic with time
    bounds, encoded in MTL~\cite{Koymans1990} or STL~\cite{MalerN04}.
    The task is to find initial conditions and time-varying inputs
    subject to given constraints that steer the system into a
    violation of the respective requirement.  This search is typically
    guided using well-established robustness
    metrics~\cite{FainekosPappas2009} that give a quantitative account
    of how close a given input is to violating a requirement.  Using such
    metrics as score functions permits one to employ standard
    optimization techniques to find falsifying inputs.  Recent results
    in falsification have produced a variety of techniques, mature
    tools, and practical applications, see~\cite{BDDFMNS2018,corso2020survey} for an
    overview.  Due to the complexity and unclear semantics of Matlab
    and Simulink models, many previous techniques are entirely
    black-box and just observe the input/output behavior of the system
    via simulations, but grey-box approaches have been developed
    recently~\cite{YaghoubiHSCC,DBLP:conf/fm/AkazakiLYDH18,waga2020falsification} to take
    some knowledge on the internals of the system into deliberation.
    This year's falsification competition featured two more tools and
    participating teams in comparison to the previous year~\cite{ernst2019arch}

    The participating tools~2019 were \STaLiRo~\cite{annpureddy2011s},
    \Breach~\cite{Donze2010},
    \FalStar~\cite{ZhangESAH2018-MCTS,ErnstSZH2018-FalStar},
    \falsify~\cite{DBLP:conf/fm/AkazakiLYDH18},
    \ARIsTEO~\cite{ARIsTEO}, and
    \zlscheck (based on Zélus~\cite{zelus}),
    in different
    configurations (Sec~\ref{sec:participants}).

	The format of the competitions was essentially that of 2019.
    It is based on a selection of benchmark models and requirements from the literature,
    for which falsifying input traces have to be found within a given maximum of simulations.
	The format of the competitions was essentially that of 2019.
    It is based on a selection of benchmark models and requirements from the literature,
    for which falsifying input traces have to be found within a given maximum of simulations.

	The 6 benchmark were those from last year, each with individual requirements,
    taken from previous competitions and from the literature (Sec~\ref{sec:benchmarks}):
	Automatic Transmission (AT),
	Fuel Control of an Automotive Powertrain (AFC),
	Neural-network Controller (NN),
	Wind Turbine (WT),
	Chasing cars (CC),
	Aircraft Ground Collision Avoidance system (F16),
	and Steam Condenser with Recurrent Neural Network Controller (SC).
    There were two different general settings for the parameterization of the search space, as described below.
    As part of the preparation for \zlscheck, there are now variants of the model written in Zélus~(see Sec~\ref{sec:participants}).


    The results (Sec~\ref{sec:results}) have been extended to include those of
    \ARIsTEO and \zlscheck, whereas the results for the remaining tools are those of~2019.
    As expected, the results  show that tools
    perform better on some benchmarks and worse on others, and that
    different tools have different strengths.
    A characteristic shared with \falsify is that the new contenders can
    find falsifying inputs ``online'' on a single trace.
    
	\section{Participants}
	\label{sec:participants}

	\paragraph{\STaLiRo.} \STaLiRo~\cite{annpureddy2011s} is a Matlab
    toolbox for monitoring and test case generation against system
    specifications presented in STL. The test cases are automatically
    generated using optimization techniques guided by formal
    requirements in STL in order to find falsifying systems behaviors.
    The tool has different optimization algorithms. Specifically, in
    this competition, the stochastic optimization with adaptive
    restarts (SOAR) \cite{mathesen2019soar} framework is used for all
    the benchmarks except for choosing instance 1 type inputs in Steam
    Condenser model. In that benchmark Simulated annealing global
    search was combined by a local optimal control based search
    \cite{YaghoubiHSCC}.  \STaLiRo is publicly available on-line under
    General Public License (GPL) \footnote{\url{
        https://sites.google.com/a/asu.edu/s-taliro}}.

	\paragraph{\Breach.} \Breach~\cite{Donze2010} is a Matlab toolbox
    for test case generation, formal specification monitoring and
    optimization-based falsification and mining of requirements for
    hybrid dynamical systems. A particular emphasis is put on
    modularity and flexibility of inputs generation, requirement
    evaluation and optimization strategy. For this work, the approach
    has been to ensure that each benchmark was properly implemented
    and a default, relatively basic falsification strategy has been
    applied. The idea was to perform a first systematic investigation
    of the proposed problems, and then to provide a base to work on
    for future editions of the competition to test a larger variety on
    approaches on the most challenging instances.  Breach is available
    under BSD license\footnote{\url{https://github.com/decyphir/breach}}.

	\paragraph{\FalStar.}
	\FalStar is an experimental prototype of a falsification tool that
    explores the idea to construct falsifying inputs incrementally in
    time, thereby exploiting potential time-causal dependencies in the
    problem.  It implements several algorithms, of which two were used
    in the competition: A two layered framework combining Monte-Carlo
    tree search (MCTS) with stochastic
    optimization~\cite{ZhangESAH2018-MCTS}, and a probabilistic
    algorithm~\cite{ErnstSZH2018-FalStar} that adapts to the
    difficulty of the problem dubbed adaptive Las-Vegas tree search
    (aLVTS).  The code is publicly available under the BSD license.%
	\footnote{\url{https://github.com/ERATOMMSD/falstar}}

	\paragraph{\falsify.}
	\falsify is an experimental program which solves falsification
    problems of safety properties by reinforcement
    learning~\cite{DBLP:conf/fm/AkazakiLYDH18}.  \falsify uses a
    \emph{grey-box} method, that is, it learns system behavior by
    observing system outputs during simulation.  \falsify is currently
    implemented by a deep reinforcement learning algorithm
    \emph{Asynchronous Advantage Actor-Critic}
    (A3C)~\cite{DBLP:conf/icml/MnihBMGLHSK16}.


		\paragraph{\ARIsTEO.} \ARIsTEO~\cite{ARIsTEO}  is a Matlab toolbox for test case generation against system specifications presented in STL and it is developed on the top of \STaLiRo .
\ARIsTEO is designed to targeting a large and practically-important category of CPS models, known
as \emph{compute-intensive} CPS (CI-CPS) models, where a single simulation of the model may take hours to complete.
\ARIsTEO embeds black-box testing into an iterative approximation-refinement loop.
At the start, some sampled inputs and outputs of the model under test are used to generate
a surrogate model that is faster to execute and can be subjected
to black-box testing.
Any failure-revealing test identified for the surrogate model is checked on the original model.
If spurious, the test results are used to refine the surrogate model to be tested again.
Otherwise, the test reveals a valid failure.
      \ARIsTEO is publicly available
    under  General Public License (GPL).\footnote{\url{https://github.com/SNTSVV/ARIsTEO}}

    \paragraph{\zlscheck.}
    \zlscheck is a tool for test case generation of programs written in Zélus\footnote{\url{http://zelus.di.ens.fr/}}~\cite{zelus}, a language reminiscent of the synchronous languages Lustre~\cite{lustre} and Scade~\cite{scade} extended in order to express ODEs. For now \zlscheck applies to the discrete-time subset of Zélus. \\
    Properties are expressed as synchronous observers~\cite{sync_obs} with a quantitative semantics to solve the falsification problem as an optimization problem.
    \zlscheck uses automatic differentiation to compute gradients of the robustness of a model w.r.t. some input parameters and uses gradient-based techniques to find a falsifying input.
    Additionally, it uses FADBADml\footnote{\url{https://fadbadml-dev.github.io/FADBADml/}}, a tool for automatic differentiation which we ported from C++ to OCaml. \\
    All the models of this competition have been rewritten manually in Zélus and are available along with the tool on github\footnote{\url{https://github.com/ismailbennani/zlscheck}}. The Simulink's Integrator block is programmed in Zélus as a fixed-step forward euler scheme. \\
    The counter-examples found by \zlscheck were systematically validated on their corresponding Simulink models, the ones that did not pass validation were discarded from the final results.%

	\section{Benchmark Definitions} \label{sec:benchmarks}

    \paragraph{Input Parameterization}

	\emph{Arbitrary piece-wise continuous input signals (Instance 1).}
    This option leaves the input specification up to the participants.
    The search space is, in principle, the entire set of piece-wise
    continuous input signals (i.e., which permit discontinuities),
    where the values for each individual
    dimensions are from a given range.  Additional constraints that
    were suggested are finite-number of discontinuity and finite
    variability for all continuous parts of inputs.  Further, each
    benchmark may impose further constraints.  Participants may
    instruct their tools to search a subset of the entire search
    space, notably to achieve finite parametrization, and then to
    apply an interpolation scheme to synthesize the input signal.

	However, the participants agreed that such a choice must be
    ``reasonable'' and should be justified from the problem's
    specification without introducing external knowledge about
    potential solutions.  Moreover, more general parametrizations that
    are shared across requirements and benchmark models were
    preferable.  Due to the diversity of benchmarks, it was decided to
    evaluate the proposed solutions using common sense.

	\emph{Constrained input signals (Instance 2).}  This option
    precisely fixes the format of the input signal, potentially
    allowing discontinuities. An example input signal would be
    piecewise constant with $k$ equally spaced control points, with
    ranges for each dimension of the input, disabling interpolation at
    Simulink input ports so that tools don't need to up-sample their
    inputs.  The arguments in favor of that are increased
    comparability of results.  As possible downside was mentioned that
    optimization-based tools (\STaLiRo and \Breach) are just compared with
    respect to their optimization algorithm.
    Nevertheless such a comparison is still meaningful, in particular,
    as as \FalStar and \falsify implement other approaches to falsification.

    A brief description of the benchmark models follows,
    the requirements are shown in Table~\ref{tab:requirements}.
    \input{requirements.tex}

	\paragraph{Automatic Transmission (AT).}

	This model of an automatic transmission encompasses a controller
    that selects a gear~1 to~4 depending on two inputs (throttle,
    brake) and the current engine load, rotations per minute~$\omega$,
    and car speed~$v$.  It is a standard falsification benchmark
    derived from a model by Mathworks and has been proposed for
    falsification in~\cite{ARCH14}.

	Input specification: $0 \le \mathit{throttle} \le 100$ and
    $0 \le \mathit{brake} \le 325$ (both can be active at the same
    time).  Constrained input signals (instance~2) permit
    discontinuities at most every 5~time units.
	Requirements are specific versions of those in \cite{ARCH14} where the parameters have been chose to be somewhat difficult. \\

    % \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
	% $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

	\paragraph{Fuel Control of an Automotive Powertrain (AFC).}

	The model is described in \cite{JinHSCC2014} and has been used in
    two previous instalments of this
    competition~\cite{ARCH17falsification,ARCH18falsification}.  The
    specific limits used in the requirements are chosen such that
    falsification is possible but reasonably hard.

	The constrained input signal (instance~2) fixes the
    throttle~$\theta$ to be piecewise constant with 10 uniform
    segments over a time horizon of 0 with two modes (normal and power
    corresponding to feedback and feedforward control), and the engine
    speed~$\omega$ to be constant with $900 \le \omega < 1100$ to
    capture the input profile outlined in \cite{JinHSCC2014} and to
    match the previous competitions.  For this reason, we do not
    consider the unconstrained (instance~1) input specification.
    Faults are disabled (e.g. by setting $\mathtt{fault\_time} > 50$).

	\paragraph{Neural-network Controller (NN).}

	This benchmark is based on MathWork's neural network controller for a system that levitates a magnet above an electromagnet at a reference position.%
	\footnote{\url{https://au.mathworks.com/help/deeplearning/ug/design-narma-l2-neural-controller-in-simulink.html}}
	It has been used previously as a falsification demonstration in the distribution of Breach.
	The model has one input, a reference value $\mathit{Ref}$ for the position, where $1 \le \mathit{Ref}$ and $\mathit{Ref} \le 3$.
	It outputs the current position of the levitating magnet~$\mathit{Pos}$.
	The input specification for instance~1 requires discontinuities to be at least 3~time units apart,
	whereas instance~2 specifies an input signal with exactly three constant segments.
	The time horizon for the problem is~40.
	The requirement ensures that after changes to the reference, the actual position eventually stabilizes around that value with small error.
    % \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
	% $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

	\paragraph{Wind Turbine (WT).}

	The model is a simplified wind turbine model proposed
    in~\cite{ARCH16:Hybrid_Modelling_of_Wind}.  The input of the
    system is wind speed $v$ and the outputs are blade pitch angle
    $\theta$, generator torque $M_{g, d}$, rotor speed $\Omega$ and
    demanded blade pitch angle $\theta_d$.  The wind speed is
    constrained by $8.0 \leq v \leq 16.0$.  Instance 1 allows any
    piece-wise continuous inputs, while instance 2 constrains inputs
    to piece-wise constant signals whose control points which are
    evenly spaced each 5 seconds.  The model is relatively large.
    Further, the time horizon is long (630) compared to other
    benchmarks.

    % \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
    % $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

	\paragraph{Chasing cars (CC).}

	The model is derived from Hu et al.~\cite{hu2000towards} which
    presents a simple model of an automatic chasing car.  Chasing cars
    (CC) model consists of five cars, in which the first car is driven
    by inputs ($\mathit{throttle}$ and $\mathit{brake}$), and other
    four are driven by Hu et al.'s algorithm.  The output of the
    system is the location of five cars $y_1, y_2, y_3, y_4, y_5$.
    The properties to be falsified are constructed artificially, to
    investigate the impact of complexity of the formulas to
    falsification.  The input specifications for instance 1 allows any
    piecewise continuous signals while the input specification for
    instance 2 constraints inputs to piecewise constant signals with
    control points for each 5 seconds, i.e., 20 segments.

%   \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
%   $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.


	\paragraph{Aircraft Ground Collision Avoidance System (F16).}
    The model has been derived
    from the one presented in~\cite{ARCH18:Verification_Challenges_in_F_16}.
    The F16
    aircraft and its inner-loop controller for Ground Collision
    avoidance have been modeled using 16 continuous variables with
    piece-wise nonlinear differential equations. Autonomous maneuvers
    are performed in an outer-loop controller that uses a finite-state
    machine with guards involving the continuous variables. The system
    is required to always avoid hitting the ground during its maneuver
    starting from all the initial conditions for roll, pitch, and yaw
    in the range
    $[0.2\pi, 0.2833\pi] \times [-0.4\pi,-0.35\pi] \times [-0.375\pi,-0.125\pi]$.%
        \footnote{Last year's report erroneously specifies:
    $[0.2\pi, 0.2833\pi] \times [-0.5\pi,-0.54\pi] \times [0.25\pi, 0.375\pi]$,
                  however, the results were in fact obtained with the correct range.
        }
    Since the benchmark has no time-varying input, there
    is no distinction between instance~1 and instance~2.  The
    requirement is checked for a time horizon equal to 15.

	\paragraph{Steam condenser with Recurrent Neural Network Controller (SC).}

	The model is presented in \cite{YaghoubiHSCC}. It is a dynamic model of an steam condenser based on energy balance and cooling water mass balance controlled with a Recurrent Neural network in feedback. The time horizon for the problem is 35 seconds. The input to the system can vary in the range $[3.99, 4.01]$.
    For instance~2, the input signal should be piecewise constant with 20~evenly spaced segments.

    % \falsify: We choose $\Delta T = 0.1$ for SC model because Instance 2 uses $\Delta T = 1.75$, which is near to $\Delta T = 1$.

	\section{Evaluation}
    \label{sec:results}

	Falsification tools were instructed to run each individual
    requirement 50 times, to account for the stochastic nature of most
    algorithms.  We report the falsification rate, i.e., the number of
    trials where a falsifying input was found, as well as the median
    and mean of the number of simulations required to find such input
    (not including the unsuccessful runs in the aggregate).  The
    cut-off for the number of simulations per trial was
    300. % and in case no falsification was achieved, we indicate a table entry ``N/A''.

    The results for unconstrained piecewise-continuous input signals (instance~1)
    are shown in Table~\ref{table:allResultsInstance1}.
    For a better comparison of the performance of the tools, a common
    ground is piecewise constant input signals (instance~2) with a
    concrete specification of the number of discontinuities allowed.
    The corresponding results are shown in Table~\ref{table:allResultsInstance2}.

    They depend on the choices for the search space,
    which we briefly discuss for each participating tool:

    \paragraph{\Breach.} For most benchmarks (exceptions detailed below), a
    piecewise constant signal generation was used with fixed step
    size. For all instances, the optimization strategy used is the
    default global Nelder Mead (GNM) approach with a custom
    configuration for the competition, resulting in the following
    three phases behavior:
    \begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
    \item Phase 1.: at most $n_{\text{corners}}= $ 64 corner samples are tested, i.e., inputs
      for which control points take only extreme values;
    \item Phase 2.: $n_{\text{quasi-rand}} = 100-n_{\text{corners}}$
      quasi-random samples from the Halton sequence with
      varying start points determined by a random seed are tested;
    \item Phase 3.: the robustness results from phase 1 and 2 are
      sorted and Nelder Mead optimization is run from the most
      promising samples.
    \end{itemize}
    Note that as a result of this approach, whenever a falsifying
    input is consistently found with less than 100 simulations, it
    indicates that the problem is likely trivially falsifiable with
    extreme inputs or a quick stochastic exploration of the search
    space. The following settings were chosen for input generation
    for each benchmark:
    \begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
    \item AT: throttle input and brake inputs were configured
      with respectively 3 and 2 control points at variable times;
    \item NN: input was piecewise constant with 3 control
      points regularly spaced;
    \item WT: spline interpolation with control
      points regurlary spaced by 5s and saturation to domain [8;16]
      (same for instance 2);
    \item CC: same as instance 2, i.e., piecewise constant input with control
      points regurlary spaced by 5s;
    \item SC: same as instance 2, i.e., piecewise constant input with control
      points regurlary spaced by 1.75s;
    \end{itemize}


    \paragraph{\STaLiRo.} In S-TaLiRo, input signals are parameterized in two ways:
    the number of control points for the input signal, and the
    time location of those control points during simulation. The number of
    control points for each input signal is given by the user forming
    an optimization problem with search space dimension the same as the number of
    control points. An option is provided to the user to add to the search space
    the timing of the control points, but this option is not used in the competition.
    For this competition, the control point time locations are evenly spaced over the duration
    of the simulation for all the benchmarks except for the SC problem instance 1.

    For the transmission model the $[\mathit{throttle}, \mathit{brake}]$ control points
    are interpolated with the \textit{pchip} function, with $[7, 3]$ as
    the number of control points in specifications 1-6 and $[4,2]$ for
    7-9 to reduce the dimensionality of the search space. For the Neural model, we use $13$
    control points to yield piecewise constant signals of $3.33$ seconds apart.
    %The Chasing Cars model used $5$ control points for piecewise constant signals of $20$ seconds each.
    The Wind Turbine used the default model input of $126$ control points interpolated linearly.
    For the SC model, Simulated Annealing (SA) global search was utilized in combination with an optimal control based local search on the infinite dimensional input space. The SA global search utilizes piecewise constant inputs with 12 possibly uneven time durations.

    \paragraph{\FalStar~(MCTS).}
    The search space included piecewise constant inputs. For all the benchmarks and for all the specifications, the number of control points was computed according to the simulation time that was divided by a fixed interval of 5 time units (e.g., a simulation time of 50 has 10 control points).

    \paragraph{\FalStar~(aLVTS).} The search space included piecewise constant
    inputs (the only parameterization currently supported), ranging
    from~2 upto~4 control points at which discontinuities are allowed
    (resp. upto~3 for NN).  In this configuration \FalStar benefits
    from a low number of control points and is more likely to try
    inputs with fewer control points first.  For the AT benchmarks it
    was clear beforehand that this choice suffices to falsify all
    benchmarks, and the setting was then kept for the remaining
    experiments.

    \paragraph{\falsify.}
	The input specification uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
	$\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.
	The choice for the SC model was $\Delta T = 0.1$ model because Instance 2 uses $\Delta T = 1.75$, which is near to $\Delta T = 1$.

	\paragraph{\ARIsTEO.} \ARIsTEO\ provides the same interface and parameters as \STaLiRo, while providing additional configuration options.
We had used an \textsc{arx} model (\textsc{arx}-$2$) with order $na=2$, $nb=2$, and $nk=2$\footnote{\url{https://nl.mathworks.com/help/ident/ref/arx.html}}
as structure for the surrogate model used in the approximation-refinement loop of \ARIsTEO .
For models with multiple inputs and outputs the dimension of the matrix $na$, $nb$ and $nk$ is changed depending on the number of inputs and outputs.
We used the default configuration of \STaLiRo\ for searching failure-revealing revealing tests on the surrogate model.
We considered the same parametrization of \STaLiRo\ for the input signals.
The original Simulink model was executed once to learn the initial surrogate model.  The cut-off values for the number of simulations of the original model and for the number of simulations of the surrogate model (per trial) were set to 300.
The results of \ARIsTEO\ can further improve by  (i)~using configurations for the surrogate model that provide more accurate approximations of the original models and more effectively guide the search toward faulty inputs; and (ii)~using the SOAR option of \STaLiRo that significantly improved the results of \STaLiRo compared with the last edition of this competition.

Properties AT51, AT52, AT53 and AT54 are currently not supported. To verify these properties with \STaLiRo\ it is necessary to embed the model into a Matlab function. Then, \STaLiRo\ performs simulation by iteratively executing the Matlab function rather than directly simulating the model. This feature is currently not supported by \ARIsTEO. We are working on removing this limitation.

    \paragraph{\zlscheck.}
    The inputs of the systems are bounded piecewise constant streams. A bounded piecewise constant stream $x$ of size $N$ and period $k$ is such that $\forall n \in [0,N], x(n) = x(\lfloor\frac{n}{k}\rfloor\cdot k)$. It is totally defined by $(\lfloor \frac{N}{k} \rfloor + 1)$ values (parameters). \\
    Two different strategies were used in these benchmarks:
    \begin{itemize}
        \item \underline{classic}: the optimization is done offline: the parameters are generated at the beginning of the simulation, then the corresponding robustness is computed. The optimization algorithm then uses the robustness and its gradient w.r.t. the parameters to compute the next parameters. \\
        This strategy has been used for properties AT1, AT2, AT6, AFC29, AFC33, NN, WT, F16 and SC.

        \item \underline{mode switch}: the optimization is performed online: for each input, the parameter number $\lfloor\frac{n}{k}\rfloor$ is generated at step $\lfloor\frac{n}{k}\rfloor\cdot k$ of the simulation. \\
        In order to achieve coverage of the different modes of the system (a mode is a state in a hierarchical automaton), \zlscheck chooses randomly an available transition and drives the system towards triggering it: it computes a quantitative interpretation of the complement of its guard (transition robustness) and the gradients of that robustness w.r.t. the current values of the inputs of the system (at step $n$ this would be the parameters number $\lfloor\frac{n}{k}\rfloor$). The value of the next parameter is then computed by the optimization algorithm using its precedent value and the gradient. \\
        Once the transition has been triggered, the tool chooses a new one randomly and so on, until the simulation ends. In this mode, the input are generated independently of the property being falsified. \\
        This strategy has been used for properties AT5, AFC27 and CC.
    \end{itemize}
    The gradient-based algorithm used by \zlscheck is a simple gradient descent with decreasing step-size: at step $l$ of the optimization, the step-size is $a(l) = a(0) / \sqrt l$ where $a(0)$ is a metaparameter. The first input is sampled uniformly in the input space, and if the same input is generated twice in a row, the algorithm restarts.
    Other gradient descent algorithms (ADAM, ADAGRAD, AMSGRAD) are implemented in \zlscheck and have been tested but did not give significantly better results. \\
    Additionally, given that the integration scheme is fixed-step, we can express the period $k$ of the inputs as times instead of number of simulation steps. For instance 1, those periods are (in seconds):
    ${\Delta T_{AT} = 0.5}$ except ${\Delta T_{AT2} = 2.5}$, ${\Delta T_{AT6a} = 5}$, ${\Delta T_{AT6b} = 10}$, ${\Delta T_{AT6c} = 15}$ and
    ${\Delta T_{AFC} = 5}$ and ${\Delta T_{NN} = 3}$ and ${\Delta T_{WT} = 5}$ and ${\Delta T_{CC} = 5}$ and ${\Delta T_{F16} = +\infty}$, ${\Delta T_{SC} = 0.2}$.


    \input{results}

    \paragraph{Discussion.}

    Several tools manage to consistently falsify using just a few simulations only (\falsify, \ARIsTEO, \zlscheck),
    in particular on benchmarks which are difficult for other approaches (NN, CC4).
    This is remarkable as all sampling and tuning of the input is done
    on the fly with respect to an established prefix.
    The results from the first few columns are discussed in more depth in~\cite{ernst2019arch}.

    \section{Conclusion and Outlook}

The benchmarks established in this competition provide a reasonable basis for comparison of methods,
which are starting to be used not only here but also in related work~\cite{eddeland2020}.%
    \footnote{\url{https://github.com/decyphir/ARCH20_ATwSS}}
This is an encouraging trend. However, clearly more difficult benchmarks are needed,
possibly with classification according to their respective difficulties.

Finally, the community should aim at reproducing the experiments and at validating whether the generated input signals are indeed falsifying traces, which needs further work.
Here, the translation of the benchmarks to Zélus is highly useful, to have an independent and open source alternative to using Matlab/Simulink as an execution engine.

\paragraph{Acknowledgments}
P. Arcaini and Z. Zhang are supported by ERATO HASUO Metamathematics for Systems Design Project (No. JPMJER1603), JST. Funding Reference number: 10.13039/501100009024 ERATO.
The report on \falsify is based on results obtained from a project commissioned by the New Energy and Industrial Technology Development Organization (NEDO).
The ASU team (\STaLiRo) was partially supported by NSF CNS 1350420, NSF CMMI 1829238, NSF IIP-1361926 and the NSF I/UCRC Center for Embedded Systems.
C. Menghi  is  supported by  the European Research Council under the European Union’s Horizon 2020 research and innovation programme (grant No 694277).
\zlscheck was funded by the ModeliScale Inria Project Lab.
	\bibliographystyle{plain}
	\bibliography{references}

\end{document}
