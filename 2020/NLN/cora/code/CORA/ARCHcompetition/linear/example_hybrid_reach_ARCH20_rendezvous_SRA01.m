function example_hybrid_reach_ARCH20_rendezvous_SRA01()
% example_hybrid_reach_ARCH20_rendezvous_SRA01 - example of linear 
% reachability analysis from the ARCH20 friendly competition 
% (instance of rendevouz example). The guard intersections are calculated
% with Girard's method
%
% Syntax:  
%    example_hybrid_reach_ARCH20_rendezvous_SRA01
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Niklas Kochdumper
% Written:      12-March-2018
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% x_1 = v_x
% x_2 = v_y
% x_3 = s_x 
% x_4 = s_y
% x_5 = t


% Parameter ---------------------------------------------------------------

params.R0 = zonotope([[-900; -400; 0; 0; 0],diag([25; 25; 0; 0; 0])]);
params.startLoc = 1;
params.tFinal = 300;


% Reachability Settings ---------------------------------------------------

% time step
options.timeStepLoc{1} = 2e-1;
options.timeStepLoc{2} = 2e-2;
options.timeStepLoc{3} = 2e-1;

% settings for continuous reachability 
options.zonotopeOrder=10; 
options.taylorTerms=3;

% settings for computing guard intersections
options.guardIntersect = 'zonoGirard';
options.enclose = {'box'};


% System Dynamics ---------------------------------------------------------

HA = rendezvous_SRA01();


% Simulation --------------------------------------------------------------

% settings for simulation
simOpt.points = 10;
simOpt.fracVert = 0.5;
simOpt.fracInpVert = 0.5;
simOpt.inpChanges = 6;

% random simulation
HA = simulateRandom(HA,params,simOpt); 


% Reachability Analysis ---------------------------------------------------

%reachable set computations
tic
HA = reach(HA,params,options);
tComp = toc;


% Verification ------------------------------------------------------------

tic
Rcont = get(HA,'reachableSet');
Rcont = Rcont.OT;
verificationCollision = 1;
verificationVelocity = 1;
verificationLOS = 1;
spacecraft = interval([-0.1;-0.1],[0.1;0.1]);

% feasible velocity region as polytope
C = [0 0 1 0 0;0 0 -1 0 0;0 0 0 1 0;0 0 0 -1 0;0 0 1 1 0;0 0 1 -1 0;0 0 -1 1 0;0 0 -1 -1 0];
d = [3;3;3;3;4.2;4.2;4.2;4.2];

% line-of-sight as polytope
Cl = [-1 0 0 0 0;tan(pi/6) -1 0 0 0;tan(pi/6) 1 0 0 0];
dl = [100;0;0];

% passive mode -> check if the spacecraft was hit
for i = 1:length(Rcont{3})    
    temp = interval(Rcont{3}{i});
    if isIntersecting(temp(1:2),spacecraft)
       verificationCollision = 0;
       break;
    end
end

% randezvous attempt -> check if spacecraft inside line-of-sight
for i = 2:length(Rcont{2})  

    temp = interval(Cl*Rcont{2}{i})-dl;

    if any(supremum(temp) > 0)
       verificationLOS = 0;
       break;
    end
end

% randezvous attempt -> check if velocity inside feasible region
for i = 1:length(Rcont{2})  

    temp = interval(C*Rcont{2}{i})-d;

    if any(supremum(temp) > 0)
       verificationVelocity = 0;
       break;
    end
end

tVer = toc;

verificationCollision
verificationVelocity
verificationLOS

disp(['computation time of verification: ',num2str(tVer)]);
disp(' ');
disp(['overall computation time: ',num2str(tVer + tComp)]);
disp(' ');




% Visualiztion ------------------------------------------------------------

warning('off','all')
figure 
hold on
box on
fill([-100,0,-100],[-60,0,60],'y','FaceAlpha',0.6,'EdgeColor','none');
options.projectedDimensions = [1 2];
options.plotType = {'b','m','g'};
plot(HA,'reachableSet',options); %plot reachable set
plotFilled(params.R0,options.projectedDimensions,'w','EdgeColor','k'); %plot initial set
% plot(HA,'simulation',options); % plot simulation
xlabel('s_x');
ylabel('s_y');

figure 
hold on
box on
fill([-3,-1.2,1.2,3,3,1.2,-1.2,-3],[-1.2,-3,-3,-1.2,1.2,3,3,1.2],'y','FaceAlpha',0.6,'EdgeColor','none');
options.projectedDimensions = [3 4];
options.plotType = {'b','m','g'};
plot(HA,'reachableSet',options); %plot reachable set
plotFilled(params.R0,options.projectedDimensions,'w','EdgeColor','k'); %plot initial set
% plot(HA,'simulation',options); % plot simulation
warning('on','all')
xlabel('v_x [m]');
ylabel('v_y [m/min]');




% Visited Locations -------------------------------------------------------

% get results
traj = get(HA,'trajectory');
locTraj = traj.loc;
setTraj = unique(locTraj)


%------------- END OF CODE --------------
