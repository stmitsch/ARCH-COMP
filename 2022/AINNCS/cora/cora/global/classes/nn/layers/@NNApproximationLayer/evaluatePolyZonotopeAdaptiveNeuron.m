function [c, G, Grest, d] = evaluatePolyZonotopeAdaptiveNeuron(obj, c, G, Grest, Es, order, ind, ind_, approx)
% ERROR: should not be called for NNApproximationLayer
% used to overwrite super class
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NNActivationLayer,
% NNApproximationlayer/evaluatePolyZonotopeAdaptive
%
% Author:        Tobias Ladner
% Written:       05-April-2022
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

error("Should not be called for NNApproximationLayer")
end