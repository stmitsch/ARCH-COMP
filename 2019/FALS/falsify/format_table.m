T1 = [[]; generate_table('NN', 3)];
T2 = [[]; generate_table('NN', 14)];
T2 = [T2; generate_table('PTC', 5)];
T1 = [T1; generate_table('SteamCondenser', 0.1)];
T2 = [T2; generate_table('SteamCondenser', 1.75)];
T1 = [T1; generate_table('WT', 1)];
T2 = [T2; generate_table('WT', 5)];
T1 = [T1; generate_table('transmission', 1)];
T2 = [T2; generate_table('transmission', 5)];
T1 = [T1; generate_table('cars', 1)];
T2 = [T2; generate_table('cars', 5)];

T1 = sortrows(T1, {'model', 'algorithm', 'Property'});
writetable(T1, 'data/result/Instance1.csv');

T2 = sortrows(T2, {'model', 'algorithm', 'Property'});
writetable(T2, 'data/result/Instance2.csv');

function [T] = generate_table(model, sampleTime)
    global logDir
    table_name = [logDir, '/', model, '.csv'];
    if isfile(table_name)
        T = readtable(table_name);
        rows = T.sampleTime == sampleTime;
        T = T(rows, :);
        T.Properties.VariableNames = {'id', 'model', 'Property', 'algorithm', 'sampleTime', 'simulations', 'time', 'robustness'};
        T.falsified = T.robustness < 0;
        for i = 1:size(T, 1)
            prop = T.Property(i);
            algorithm = T.algorithm(i);
            id = T.id(i);
            filename = [logDir, '/', model, '-', prop{1}, '-', algorithm{1}, '-', num2str(id), '.mat'];
            load(filename, 'bestXout', 'bestOpts');
            if (isequal(model, 'SteamCondenser'))
                bestXout = timeseries(squeeze(bestXout.Data), bestXout.Time);
            end
            if (isequal(model, 'PTC'))
                T.opt(i) =  bestOpts{2};
            else
                T.opt(i) = 0;
            end
            control_points = 0:T.sampleTime(i):bestXout.Time(end);
            X = resample(bestXout, control_points);
            T.contol_points(i) = {mat2str(control_points)};
            inputs = reshape(X.Data, [size(X.Data,1),size(X.Data,3)]);
            T.input(i) = {mat2str(inputs')};
        end
    else
        T = [];
    end
end
    
