function inputArgsCheck(inputArgs)
% inputArgsCheck - checks input arguments of CORA functions for compliance
%    with semantic requirements; if any violation is detected, an error is
%    thrown, otherwise nothing happens
%
% Syntax:  
%    inputArgsCheck(inputArgs)
%
% Inputs:
%    inputArgs - cell structure according to format
%                  { {input1,id,{attributes}};
%                    {input2,id,{possibilities}};
%                     ...; };
%                to describe how input arguments should be like:
%                   input1, input2, ... - value of input argument
%                   id - identifier for attribute check ('att') or string
%                        comparison ('str')
%                   attributes - (only id = 'att') admissible types (check
%                                MATLAB function validateattributes) as a
%                                cell-array
%                   possibilities - (only id = 'str') admissible strings as
%                                   a cell-array
%
% Outputs:
%    ---
%
% Example:
%    obj = capsule(zeros(2,1),ones(2,1));
%    N = 5;
%    type = 'standard';
%    inputArgs = { {obj, 'att',{'cell','capsule','other'},{'nonempty'}};
%                  {N,   'att',{'numeric'},               {'positive'}};
%                  {type,'str',{'standard','gaussian'}               } };
%    inputArgsCheck(inputArgs);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mingrui Wang
% Written:      30-May-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% turn off warnings (if no attributes are given)
warOrig = warning;
warning('off','all');

% number of input arguments
n = length(inputArgs); 

% loop over all input arguments
for i = 1:n
    % read information about i-th input argument
    input_arg = inputArgs{i};
    % read value of i-th input argument
    arg_name = input_arg{1};
    % read identifier ('att' or 'str')
    identifier = input_arg{2};

    % case destinction
    switch identifier
        case 'att'    % check classname and attributes in this case

            % class of i-th input argument
            class = input_arg{3};
            % required attributes for i-th input argument
            attribute = input_arg{4};
            % number of attributes (can be zero)
            n_attribute = length(attribute);
            if n_attribute == 1 && strcmp(attribute{1},'')
                n_attribute = 0;
            end

            % check attribute using built-in MATLAB function
            try
                validateattributes(arg_name,class,attribute);
            catch
                % generate string of admissible values (user info)
                n_class = length(class);
                validrange = "";
                for c=1:n_class
                    validrange = validrange + string(class{c});
                    if n_attribute > 0
                        validrange = validrange + " (" + ...
                            strjoin(string(attribute),', ') + ")";
                    end
                    if c < n_class
                        % 17 white spaces for alignment
                        validrange = validrange + ",\n                   ";
                    end
                end

                % throw error
                throw(CORAerror('CORA:wrongValue',countingNumber(i),...
                    validrange));
            end

        case 'str'    % check the strings in this case

            % read string
            validateStr = input_arg{3};

            % check attribute using built-in MATLAB function
            try
                validatestring(arg_name,validateStr);
            catch
                % generate string of admissible values (user info)
                validrange = ['''', strjoin(validateStr,"', '"), ''''];

                % throw error
                throw(CORAerror('CORA:wrongValue',countingNumber(i),...
                    validrange));
            end

    end

end

% turn warnings back on
warning(warOrig);

end


% Auxiliary function ------------------------------------------------------
function text = countingNumber(i)
% returns counting number in text form
switch i
    case 1
        text = 'first';
    case 2
        text = 'second';
    case 3
        text = 'third';
    case 4
        text = 'fourth';
    case 5
        text = 'fifth';
    case 6
        text = 'sixth';
    case 7
        text = 'seventh';
    case 8
        text = 'eighth';
    case 9
        text = 'ninth';
    otherwise
        text = ['No. ', num2str(i)];
end

end

%------------- END OF CODE --------------