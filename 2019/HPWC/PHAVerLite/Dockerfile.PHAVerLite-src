FROM ubuntu:latest

MAINTAINER enea.zaffanella@unipr.it

RUN apt-get -y update
RUN apt-get -y install apt-utils software-properties-common wget
RUN apt-get -y install g++ make autoconf automake libtool
RUN apt-get -y install bison flex libgmp-dev libmpfr-dev libflint-dev

RUN mkdir /usr/PHAVerLite
WORKDIR /usr/PHAVerLite

# Download PPLite 0.4.1 sources
RUN wget http://www.cs.unipr.it/~zaffanella/PPLite/releases/pplite-0.4.1.tar.gz
RUN tar zxf pplite-0.4.1.tar.gz

# Download PHAVerLite 0.1 sources
RUN wget http://www.cs.unipr.it/~zaffanella/PPLite/releases/phaverlite-0.1.tar.gz
RUN tar zxf phaverlite-0.1.tar.gz

# Download arch-comp19 models and config files
RUN wget http://www.cs.unipr.it/~zaffanella/PPLite/releases/arch-comp19.tar.gz
RUN tar zxf arch-comp19.tar.gz

# Build and install PPLite 0.4.1
RUN mkdir -p build/pplite
WORKDIR /usr/PHAVerLite/build/pplite
RUN ../../pplite-0.4.1/configure --enable-optimization=sspeed
RUN make -j2 && make install
WORKDIR /usr/PHAVerLite

# Build and install PHAVerLite 0.1
RUN mkdir -p build/phaverlite
WORKDIR /usr/PHAVerLite/build/phaverlite
RUN ../../phaverlite-0.1/configure --disable-assertions --disable-debugging --enable-optimization=sspeed --with-cxxflags='-s'
RUN make -j2
WORKDIR /usr/PHAVerLite

# Create symbolic link to stand-alone executable
RUN ln -s build/phaverlite/src/phaverlite_static phaverlite-0.1_static

# Run all tests and then filter times from output file
# RUN ./run-all.sh > arch-comp19.out 2>&1
# RUN grep -E '(===|Time in PHAV)' arch-comp19.out > arch-comp19-times.out
