Theorem "Wien bridge oscillator".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2<=1/2&y^2<=1/3->[{x'=-x-1117*y/500+439*y^3/200-333*y^5/500,y'=x+617*y/500-439*y^3/200+333*y^5/500&true}]x-4*y < 8
End.

Tactic "Scripted proof".
/* invariant not generated by Pegasus */
  implyR(1);
  dC({`x^2 + x*y + y^2 - 111/59 <= 0`}, 1); doall(ODE(1)); done
End.
End.

Theorem "Locally stable nonlinear system".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2<=1/2&(y+2)^2<=1/3->[{x'=-x+y-x^3,y'=-x-y+y^2&true}](-1+x)^2+(- 3/2+y)^2>1/4
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  implyR(1);
  dC({`2*x^2 + (y + 3/2)^2 - 4 <= 0`}, 1); doall(ODE(1)); done
End.
End.

Theorem "MIT astronautics Lyapunov".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2<=1/2&y^2<=1/3->[{x'=y-x^7*(x^4+2*y^2-10),y'=-x^3-3*y^5*(x^4+2*y^2-10)&true}](-2+x)^2+(-3+y)^2>1/4
End.

Tactic "Automated proof".
master
End.
End.

Theorem "1D Saddle Node".

ProgramVariables.
  R r.
  R x.
  R f.
End.

Problem.
  r<=0->\exists f (x=f->[{x'=r+x^2&true}]x=f)
End.

Tactic "Scripted proof".
implyR(1); cutR({`\exists sr sr^2=-r`},1); <(
  QE
  ,
  implyR(1);
  existsL('Llast);
  existsR({`sr`}, 1);
  implyR(1);
  dC({`x-sr=0`},1); <(
    dW(1); QE,
    dbx({`x+sr`},1)
  )
)
End.
End.

Theorem "Ahmadi Parrilo Krstic".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  0.5<=x&x<=0.7&0<=y&y<=0.3->[{x'=-x+x*y,y'=-y&true}](!(-0.8>=x&x>=-1&-0.7>=y&y>=-1))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Alongi Nelson Ex_4_1_9 page 143".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x=1&y=0&z=0->[{x'=x*z,y'=y*z,z'=-x^2-y^2&x^2+y^2+z^2=1}](!(x>1|z>0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_1_29 page 14".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0&y=-1->[{x'=x,y'=y^2&true}](!y>0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_1_30 page 14".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=0->[{x'=y^2,y'=x&true}](!x < 0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_1_31 page 14".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=-1&y=1->[{x'=x^2,y'=(2*x-y)*y&true}](!(x>0|y<=0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_1_32 page 14".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=-1&y=1->[{x'=-x*y,y'=x^2+y^2&true}](!(x>0|y<=0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_1_35 page 17".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=1->[{x'=x*(2-x-2*y),y'=(2-2*x-y)*y&true}](!(x<=0|y<=0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_3_1 page 72".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2<=1/5->[{x'=-4*y+x*(1-x^2-y^2)-y*(x^2+y^2),y'=4*x+y*(1-x^2-y^2)+x*(x^2+y^2)&true}](!x^2+y^2>1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_3_5c page 79".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2>=1->[{x'=x-y^3,y'=x^3+y&true}](!x^2+y^2 < 1/2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_3_5e page 79".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=-1->[{x'=x^2+(x+y)/2,y'=(-x+3*y)/2&true}](!y>0)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`y - x + 1 <=0 & y<=0`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Arrowsmith Place Fig_3_8 page 82".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=-1->[{x'=x*(x+2*y),y'=y*(2*x+y)&true}](!(x < 0|y>0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_3_9 page 83".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=-1&y=-1->[{x'=x*(x-2*y),y'=-(2*x-y)*y&true}](!(x>0|y>0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Arrowsmith Place Fig_3_11 page 83".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=1/8->[{x'=x-y^2,y'=y*(x-y^2)&true}](!x < 0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Ben Sassi Girard 20104 Moore-Greitzer Jet".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/5000+(1/20+x)^2+(3/200+y)^2<=0->[{x'=-3*x^2/2-x^3/2-y,y'=3*x-y&true}](!49/100+x+x^2+y+y^2<=0)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.073036*x^6-0.014461*x^5*y+0.059693*x^4*y^2-0.0063143*x^3*y^3+0.029392*x^2*y^4+0.0036316*y^6+0.064262*x^5+0.24065*x^4*y-0.082711*x^3*y^2+0.28107*x^2*y^3-0.015542*x*y^4+0.036437*y^5+0.47415*x^4-0.56542*x^3*y+1.1849*x^2*y^2-0.22203*x*y^3+0.19053*y^4-0.59897*x^3+1.8838*x^2*y-0.59653*x*y^2+0.47413*y^3+1.0534*x^2-0.51581*x*y+0.43393*y^2-0.35572*x-0.11888*y-0.25586<=0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Ben Sassi Girard Sankaranarayanan 2014 Fitzhugh-Nagumo".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/20+(5/4+x)^2+(-5/4+y)^2<=0->[{x'=7/8+x-x^3/3-y,y'=2*(7/10+x-4*y/5)/25&true}](!36/5+5*x+x^2+2*y+y^2<=0)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.12152*x^4+0.22807*x^3*y+0.214*x^2*y^2-0.71222*y^4-0.27942*x^3-0.48799*x^2*y-0.2517*x*y^2-0.3366*y^3-0.21526*x^2+0.16728*x*y-0.44613*y^2+0.35541*x-0.21594*y-0.72852<=0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Bhatia Szego Ex_2_4 page 68".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+(-1/2+y)^2 < 1/24->[{x'=-x+2*x^3*y^2,y'=-y&x^2*y^2 < 1}](!(x<=-2|y<=-1))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.00028346*x^6*y^3 - 0.00049999*x^5*y^4 + 0.00018784*x^4*y^5 + 0.0002836*x^3*y^6 - 0.00020211*x^2*y^7 - 0.00011251*x*y^8 + 0.00032872 *y^9 - 0.00032162*x^7*y - 0.0012954*x^6*y^2 - 0.0038775*x^5*y^3 - 0.0085289*x^4*y^4 + 0.0016293*x^3*y^5 + 0.0018949*x^2*y^6 - 0.0035577 *x*y^7 + 0.011859*y^8 - 0.00043993*x^7 - 0.0050214*x^6*y - 0.0047617*x^5 *y^2 + 0.011009*x^4*y^3 + 0.012178*x^3*y^4 + 0.0055785*x^2*y^5 + 0.0037099*x*y^6 - 0.062198*y^7 - 0.0023971*x^6 + 0.005244*x^5*y + 0.0031699*x^4*y^2 + 0.00069299*x^3*y^3 + 0.14159*x^2*y^4 - 0.081176*x *y^5 + 0.27277*y^6 + 0.0089006*x^5 + 0.10703*x^4*y + 0.0021026*x^3*y^2 - 0.39217*x^2*y^3 + 0.17202*x*y^4 - 0.85217*y^5 + 0.052331*x^4 - 0.008468*x^3*y - 0.21988*x^2*y^2 - 0.19426*x*y^3 + 1.3672*y^4 - 0.067608*x^3 - 1.3917*x^2*y - 1.3541*x*y^2 - 1.0593*y^3 - 0.80894*x^2 - 2.9459*x*y + 1.0078*y^2 - 1.8819*x - 2.2726*y - 1.433 <= 0 & y>=0 & x>=-2`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Collin Goriely page 60".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (2+x)^2+(-1+y)^2<=1/4->[{x'=x^2+2*x*y+3*y^2,y'=2*y*(2*x+y)&true}](!x>0)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  implyR(1); dC({`y>=0 & x+y<=0`},1); <(dW(1); QE, master)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dai Gan Xia Zhan JSC14 Ex. 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  1/100-x^2-y^2>=0->[{x'=-2*x+x^2+y,y'=x-2*y+y^2&true}](!x^2+y^2>=1/4)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dai Gan Xia Zhan JSC14 Ex. 2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+(2+y)^2<=1->[{x'=2*x-x*y,y'=2*x^2-y&true}](!x^2+(-1+y)^2<=9/100)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.0052726*x^10+0.013182*x^8*y^2+0.013181*x^6*y^4+0.0065909*x^4*y^6+0.0016477*x^2*y^8+0.00016477*y^10-0.060426*x^8*y-0.11666*x^6*y^3-0.08401*x^4*y^5-0.02829*x^2*y^7-0.0026618*y^9-0.0093935*x^8+0.25715*x^6*y^2+0.35556*x^4*y^4+0.18385*x^2*y^6+0.017843*y^8-0.22922*x^6*y-0.82409*x^4*y^3-0.6654*x^2*y^5-0.072582*y^7+0.38533*x^6+1.6909*x^4*y^2+1.7759*x^2*y^4+0.20099*y^6+1.8855*x^4*y-0.83113*x^2*y^3-0.10854*y^5-4.9159*x^4-11.581*x^2*y^2-1.9047*y^4+6.644*x^2*y+7.8358*y^3+1.5029*x^2-13.2338*y^2+10.8962*y-3.4708<=0&0.10731*x^10+0.26827*x^8*y^2+0.26827*x^6*y^4+0.13413*x^4*y^6+0.033534*x^2*y^8+0.0033532*y^10-1.2677*x^8*y-2.4914*x^6*y^3-1.8208*x^4*y^5-0.59588*x^2*y^7-0.057773*y^9-0.82207*x^8+4.1107*x^6*y^2+6.7924*x^4*y^4+3.4828*x^2*y^6+0.36938*y^8+6.8306*x^6*y-0.93431*x^4*y^3-5.9328*x^2*y^5-0.95223*y^7+2.2556*x^6-17.4284*x^4*y^2-6.4448*x^2*y^4-0.33741*y^6-1.2936*x^4*y+16.8675*x^2*y^3+8.8828*y^5-16.1915*x^4-39.7751*x^2*y^2-25.8126*y^4+43.7284*x^2*y+39.2116*y^3-12.7866*x^2-33.0675*y^2+15.2878*y-3.1397<=0`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Dai Gan Xia Zhan JSC14 Ex. 5".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (1+x)^2+(-2+y)^2<=4/25->[{x'=y,y'=2*x-x^3-y-x^2*y&true}](!(-1+x)^2+y^2<=1/25)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.23942*x^6 + 0.097208*x^5*y + 0.06013*x^4*y^2 - 0.0076888*x^3*y^3
  - 0.022097*x^2*y^4 + 0.067444*x*y^5 + 0.063249*y^6 - 0.11511*x^5
  - 0.093461*x^4*y - 0.061763*x^3*y^2 + 0.065902*x^2*y^3 + 0.053766*x*y^4
  - 0.1151*y^5 - 0.95442*x^4 + 0.38703*x^3*y + 0.46309*x^2*y^2 - 0.14691*x
  *y^3 + 0.11756*y^4 - 0.021196*x^3 - 0.40047*x^2*y - 0.28433*x*y^2
  - 0.028468*y^3 - 0.020192*x^2 - 0.37629*x*y - 0.13713*y^2 + 1.9803*x
  - 1.4121*y - 0.51895<=0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Darboux Christoffel Int Goriely page 58".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2<=1->[{x'=3*(-4+x^2),y'=3+x*y-y^2&true}](!(x < -4|y < -4|x>4|y>4))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.0013138*x^5 + 0.00048141*x^4*y + 0.000828*x^2*y^3 - 0.0016748*x*y^4
  + 0.0008106*y^5 + 0.010722*x^4 - 0.0018729*x^3*y + 0.0041383*x^2*y^2
  - 0.013911*x*y^3 + 0.0085091*y^4 - 0.039948*x^3 - 0.0060006*x^2*y
  - 0.046355*x*y^2 + 0.054433*y^3 - 0.028132*x^2 + 0.13217*x*y + 0.10916
  *y^2 + 0.62004*x - 0.88775*y - 1.1161<=0 &  -0.00011438*x^4*y^2 + 0.00015105*x^2*y^4 - 0.0018063*x^5 + 0.0012699*x^3
  *y^2 + 0.0014498*x^2*y^3 - 0.0014334*x*y^4 + 0.0013001*y^5 + 0.017567
  *x^4 + 0.0050023*x^3*y - 0.0016674*x^2*y^2 - 0.015315*x*y^3 + 0.01038
  *y^4 - 0.072259*x^3 - 0.035874*x^2*y - 0.050558*x*y^2 + 0.058708*y^3
  - 0.05097*x^2 + 0.042626*x*y + 0.19257*y^2 + 1.3148*x + 0.014613*y
  - 1.2585 <= 0 & x^2 + y^2 - 16 <= 0`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Djaballah Chapoutot Kieffer Bouissou 2015 Ex. 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/20+(5/4+x)^2+(-5/4+y)^2<=0->[{x'=x+y,y'=x*y-y^2/2&true}](!(5/2+x)^2+(-4/5+y)^2<=1/20)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.10316*x^4 - 0.44099*x^3 + 0.7506*x^2*y - 0.3745*x*y^2 + 0.21087*y^3 - 0.35638*x^2 - 0.16318*x*y - 0.87072*y^2 + 0.44863*x - 0.6858*y - 1.4514 <= 0`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Dumortier Llibre Artes Ex. 1_9a".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1/2&x < -1/3&y < 0&y>=-1/2->[{x'=x*(1-x^2-y^2)+y*((-1+x^2)^2+y^2),y'=y*(1-x^2-y^2)-y*((-1+x^2)^2+y^2)&true}](!x>=0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 1_9b".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a>-1/2&a < -1/3&y < 0&y>=-1/2->[{a'=a*(1-a^2-y^2)+y*((-1+a^2)^2+y^2),y'=y*(1-a^2-y^2)-y*((-1+a^2)^2+y^2)&true}](!a^2+2*y^2>2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 1_11a".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1/2&x < -1/3&y < 0&y>=-1/2->[{x'=2*x-x^5-x*y^4,y'=y-x^2*y-y^3&true}](!x+y>0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 10_11b".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1+x)^2+(1+y)^2 < 1/4->[{x'=1+x+x^2+x^3+2*y+2*x^2*y,y'=-y+2*x*y+x^2*y+2*x*y^2&true}](!y>=1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 5_1_ii".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1&x < 0&y < 0&y>=-1->[{x'=-1+x^2+y^2,y'=5*(-1+x*y)&true}](!x+y>1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 5_2_ii".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-4/5&x < -1/3&y < 0&y>=-1->[{x'=2*x-2*x*y,y'=-x^2+2*y+y^2&true}](!(x=0&y=0|x+y>1))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 5_13".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1/2&x < 0&y < 0&y>=-1/2->[{x'=y,y'=2*(-1-y)*y&true}](!y>2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 10_9".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1+x)^2+(1+y)^2 < 1/4->[{x'=x^4+2*x*y^2-6*x^2*y^2+y^4+x*(x^2-y^2),y'=2*x^2*y-4*x^3*y+4*x*y^3-y*(x^2-y^2)&true}](!y>=1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 10_11".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2 < 1/4->[{x'=-70-100*x+70*x^2+100*x^3-200*y+200*x^2*y,y'=146*x+100*y+140*x*y+100*x^2*y+200*x*y^2&true}](!(2*x>=3|x<=-3/2))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 1_11b".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a>-1/2&a < -1/3&y < 0&y>=-1/2->[{a'=2*a-a^5-a*y^4,y'=y-a^2*y-y^3&true}](!a^2+y^2>5)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 10_15_i".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1&x < -3/4&y<=3/2&y>=1->[{x'=-42*x^7+50*x^2*y+156*x^3*y+258*x^4*y-46*x^5*y+68*x^6*y+20*x*y^6-8*y^7,y'=y*(1110*x^6-3182*x^4*y-220*x^5*y+478*x^3*y^3+487*x^2*y^4-102*x*y^5-12*y^6)&true}](!x>1+y)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 10_15_ii".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1&x < -1/2&y<=-1/10&y>=-3/10->[{x'=315*x^7+477*x^6*y-113*x^5*y^2+301*x^4*y^3-300*x^3*y^4-192*x^2*y^5+128*x*y^6-16*y^7,y'=y*(2619*x^6-99*x^5*y-3249*x^4*y^2+1085*x^3*y^3+596*x^2*y^4-416*x*y^5+64*y^6)&true}](!x>1+y)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Dumortier Llibre Artes Ex. 5_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1&x < -1/2&y < -1/2&y>=-1->[{x'=y,y'=x^5-x*y&true}](!x+y>1)
End.

Tactic "Scripted proof (1)".
/* Invariant not generated by Pegasus */
  implyR(1); dC({`x+1/2<=0 & y+1/2<=0`},1); <(dW(1); QE, ODE(1))
End.

Tactic "Scripted proof (2)".
implyR(1);
  dC({`x^5+-1*x*y+-4*x^3*y<=0&y<=0`},1); <(nil, ODE(1));
  dC({`x^5+-1*x*y+-4*x^3*y<=0`},1); <(nil, ODE(1));
  dC({`y<=0`},1); <(nil, ODE(1));
  dW(1); QE
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Fitzhugh Nagumo Ben Sassi Girard 2".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  -1<=a&a<=-0.5&1<=y&y<=1.5->[{a'=7/8+a-a^3/3-y,y'=2*(7/10+a-4*y/5)/25&true}](!(-2.5<=a&a<=-2&-2<=y&y<=-1.5))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.12152*a^4+0.22807*a^3*y+0.214*a^2*y^2-0.71222*y^4-0.27942*a^3-0.48799*a^2*y-0.2517*a*y^2-0.3366*y^3-0.21526*a^2+0.16728*a*y-0.44613*y^2+0.35541*a-0.21594*y-0.72852<=0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Forsman Phd Ex 6_1 page 99".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+(-2+y)^2 < 1/24->[{x'=-x+2*x^2*y,y'=-y&true}](!(x<=-2|y<=-1))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Forsman Phd Ex 6_14 page 119".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+(-1+y)^2 < 1/8->[{x'=-2*x+y^4,y'=-y+3*x*y^3&true}](!y<=-1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Hamiltonian System 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (2/3+x)^2+y^2<=1/24->[{x'=-2*y,y'=-2*x-3*x^2&true}](!x>0)
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-(x^2+x^3-y^2)<0 & x<0`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Invariant 3-dim sphere".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x=1/4&y=1/8&z=1/10->[{x'=x^2-x*(x^3+y^3+z^3),y'=y^2-y*(x^3+y^3+z^3),z'=z^2-z*(x^3+y^3+z^3)&true}](!(x>10|y>5|z<=-20))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x^2+y^2+z^2<=1`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "KeYmaera Nonlinear Diffcut".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^3>=-1&y^5>=0->[{x'=(-3+x)^4+y^5,y'=y^2&true}](!(1+x < 0|y < 0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "KeYmaera Nonlinear 1".

ProgramVariables.
  R x.
  R a.
End.

Problem.
  x^3>=-1->[{x'=a+(-3+x)^4,a'=0&a>=0}](!x^3 < -1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Liu Zhan Zhao Emsoft11 Example 25".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x+y>=0->[{x'=-2*y,y'=x^2&true}](!(x < -1&y<=-1/2))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x+y>=-1`},1)&<( (dW(1)&QE), master)))
End.
End.

Theorem "Lotka Volterra Fourth Quadrant".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0&y=1->[{x'=x*(1-y),y'=-(1-x)*y&x>=0&y>=0}](!y < 0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Man Maccallum Goriely Page 57".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2<=1/4->[{x'=-y+2*x^2*y,y'=y+2*x*y^2&true}](!x>3)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Prajna PhD Thesis 2-4-1 Page 31".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-3/2+x)^2+y^2<=1/4->[{x'=x,y'=-x+x^3/3-y&true}](!(1+x)^2+(1+y)^2<=4/25)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Shimizu Morioka System".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x=5&y=3&z=-4->[{x'=y,y'=x-y-x*z,z'=x^2-z&true}](!z < -5)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Stable Limit Cycle 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2 < 1/16->[{x'=x-x^3+y-x*y^2,y'=-x+y-x^2*y-y^3&true}](!(x<=-2|y>2))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Stable Limit Cycle 2".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a^2+y^2 < 1/16->[{a'=a-a^3+y-a*y^2,y'=-a+y-a^2*y-y^3&true}](!(a < -1|y < -1|a>1|y>1))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`a^2+y^2<=1`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Exercise 6_1_5".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+2*(-1/3+y)^2 < 1/25->[{x'=x*(2-x-y),y'=x-y&true}](!(x>=2|x<=-2))
End.

Tactic "Scripted proof (1)".
/* Invariant not generated by Pegasus */
  implyR(1); dC({`x>=0 & y > 0 & x<2`},1); <( dW(1); QE, master)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Exercise 6_1_9 Dipole".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+y^2 < 1/25->[{x'=2*x*y,y'=-x^2+y^2&true}](!x<=-2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Example 6_3_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-4/5&x < -1/3&y < 3/2&y>=1->[{x'=-x+x*(x^2+y^2),y'=x+y*(x^2+y^2)&true}](!(x < -1/3&y>=0&2*y < 1&x>-4/5))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Exercise 6_6_1 Reversible System".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=(1-x^2)*y,y'=1-y^2&true}](!(x>=2|x<=-2))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Exercise 6_6_2 Limit Cycle".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=y,y'=-x+y*(1-x^2-y^2)&true}](!(x^2+y^2=0|x>=2|x<=-2))
End.

Tactic "Scripted proof".
/* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x^2 + y^2 - 2 <=0 & -(x^2 + y^2 - 0.01) <= 0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Example 6_8_3".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  2*(-1/3+x)^2+y^2 < 1/16->[{x'=x^2*y,y'=x^2-y^2&true}](!x<=-2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Strogatz Exercise 7_3_5".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=-x-y+x*(x^2+2*y^2),y'=x-y+y*(x^2+2*y^2)&true}](!(x=0&y=0|x<=-2|y<=-1))
End.

Tactic "Scripted proof".
/* invariant not generated by Pegasus */
  (implyR(1)&(dC({`-4/5 + x^2 + 2*x*y/5 + 7*y^2/5 <= 0 & x^2+y^2!=0`},1)&<( (dW(1)&QE), master)))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Synthetic Example 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0&y=0->[{x'=-1+x^2+y^2,y'=x^2&true}](!y < 0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Unstable Unit Circle 1".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a=0&y=1/2->[{a'=-a+a^3-y+a*y^2,y'=a-y+a^2*y+y^3&true}](!a^2+y^2>1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Unstable Unit Circle 2".

ProgramVariables.
  R b.
  R y.
End.

Problem.
  b=1/2&y=1/2->[{b'=-b+b^3-y+b*y^2,y'=b-y+b^2*y+y^3&true}](!b^2+y^2>2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Unstable Unit Circle 3".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0&y=1/2->[{x'=-x+x^3-y+x*y^2,y'=x-y+x^2*y+y^3&x>=0&y>=0}](!x^2+y^2>1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Van der Pol Fourth Quadrant".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0&y=1->[{x'=y,y'=-x+(1-x^2)*y&x<=0&y>=0}](!x^2+y^2>10)
End.


End.

Theorem "Vinograd System Chicone 1_145 Page 80".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1&y=0->[{x'=y^5+x^2*(-x+y),y'=y^2*(-2*x+y)&true}](!(y>0|y < 0))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Wiggins Example 17_1_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=(2+x)*(-(1-x)*x+y),y'=-y&true}](!x<=-5/2)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Wiggins Example 18_1_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=-x^6-x*y,y'=x^2-y&true}](!y<=-1)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Wiggins Example 18_7_3_n".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3+x)^2+(-1/3+y)^2 < 1/16->[{x'=-x+2*y+x^2*y+x^4*y^5,y'=-y-x^4*y^6+x^8*y^9&true}](!(x<=-1|y<=-1))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Invariant Clusters Example 1+2+3".
Definitions.
  R invcluster(R,R,R,R) = ( ._2-._3*(._0^2-._1^2) ).
End.

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=4&y=2->[{x'=y^2,y'=x*y&true}]x^2-y^2>=12
End.

Tactic "Manual proof".
implyR(1) ; cut({`\exists u1 \exists u3 (u1^2+u3^2!=0 & invcluster(x,y,u1,u3)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,u1,u3)=0`}, 1); <(
      dW(1) ; QE,
      dI(1)
    )
    ,
    hideR(1) ; QE
  )
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Invariant Clusters Example 4".
Definitions.
  R invcluster(R,R,R,R,R,R) = ( (-._4-._5)*._0^2+._4*._1^2+._5*._2^2+._3 ).
End.

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x=1&y=2&z=3->[{x'=y*z,y'=x*z,z'=x*y&true}](!(x=5&y^2=27&z^2=34))
End.

Tactic "Manual proof".
implyR(1) ; cut({`\exists u0 \exists u5 \exists u6 (u0^2+u5^2+u6^2!=0 & invcluster(x,y,z,u0,u5,u6)=0)`}) ; <(
    existsL('Llast)*3 ; dC({`invcluster(x,y,z,u0,u5,u6)=0`}, 1) ; doall(ODE(1)) ; done
    ,
    hideR(1) ; QE
  )
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Invariant Clusters Example 5".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x=1&y=2&z=3->[{x'=y*z,y'=x*z,z'=x*y&true}](!(x=5&y^2=27&z^2=34))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Invariant Clusters Example 6".
Definitions.
  R invcluster(R,R,R,R) = ( ._2-._3*(._0^2-._1^2) ).
End.

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (x+15)^2+(y-17)^2-1<=0->[{x'=y^2,y'=x*y&true}](!(x-11)^2+(y-16.5)^2-1<=0)
End.

Tactic "Scripted proof (1)".
implyR(1) ; cut({`\exists u1 \exists u3 (u1^2+u3^2!=0 & invcluster(x,y,u1,u3)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,u1,u3)=0`}, 1) ; <(dW(1) & QE, ODE(1)) ; done
    ,
    hideR(1) ; QE
  )
End.

Tactic "Scripted proof (2)".
implyR(1); dC({`x^2-y^2 - (old(x)^2-old(y)^2) = 0`}, 1); doall(ODE(1)); done
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Invariant Clusters Example 7".
Definitions.
  R invcluster(R,R,R,R) = ( 1/3*._3*._0^3-1/3*._3*._1^3+._3*._0^2+._3*._1^2+._2 ).
End.

ProgramVariables.
  R x.
  R y.
End.

Problem.
  1-(x+6)^2-(y+6)^2>=0->[{x'=y^2-2*y,y'=x^2+2*x&true}](!1-(x-8.2)^2-(y-4)^2>=0)
End.

Tactic "Scripted proof (1)".
implyR(1) ; cut({`\exists u1 \exists u3 (u1^2+u3^2!=0 & invcluster(x,y,u1,u3)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,u1,u3)=0`}, 1) ; <(
      dW(1) ; QE,
      dI(1)
    )
    ,
    hideR(1) ; QE
  )
End.

Tactic "Scripted proof (2)".
/* disprovable */ /*invcluster(x,y,-3081.9,7.1798)=0*/
  implyR(1); dC({`invcluster(x,y,-3081.9,21.539)<=0`}, 1);
  doall(ODE(1)); done
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Looping Particle".
Definitions.
  R r().
  R g().
  R invcluster(R,R,R,R,R,R) = ( ._5*._0^2+._5*._1^2+._4*._2^2+2*._4*g/r^2*._1+._3 ).
End.

ProgramVariables.
  R omega.
  R x.
  R y.
End.

Problem.
  x=2&y=0&x^2+y^2=r()^2&r()!=0->[{x'=-y*omega,y'=x*omega,omega'=-g()/r()^2*x&true}]x^2+y^2=r()^2
End.

Tactic "Manual proof".
implyR(1) ; cut({`\exists u0 \exists u2 \exists u5 (u0^2+u2^2+u5^2!=0 & invcluster(x,y,omega,u0,u2,u5)=0)`}) ; <(
    existsL('Llast)*3 ; dC({`invcluster(x,y,omega,u0,u2,u5)=0`}, 1) ; doall(ODE(1)) ; done
    ,
    hideR(1) ; QE
  )
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Coupled Spring-Mass System (I)".
Definitions.
  R m2().
  R k1().
  R k().
  R m1().
  R u8().
  R u1().
  R u10().
  R invcluster(R,R,R,R) = ( u8*._2*._3+k2*._0*._1*(m1*u8-2*m2*u10)/(m1*m2)+u10*._2^2+u1+1/2*._3^2*(k1*m2*u8-k2*m1*u8+k2*m2*u8+2*k2*m2*u10)/(k2*m1)+1/2*(2*k1*m2*u10-k2*m1*u8+2*k2*m2*u10)*._0^2/(m1*m2)+1/2*(k1*m2*u8-k2*m1*u8+2*k2*m2*u10)*._1^2/(m1*m2) ).
  R k2().
End.

ProgramVariables.
  R x1.
  R v1.
  R x2.
  R v2.
End.

Problem.
  k1()=k()&k2()=k()&m1()=5*m2()&m2()>0&k()!=0&u8()*v1*v2+k2()*x1*x2*(m1()*u8()-2*m2()*u10())/(m1()*m2())+u10()*v1^2+u1()+1/2*v2^2*(k1()*m2()*u8()-k2()*m1()*u8()+k2()*m2()*u8()+2*k2()*m2()*u10())/(k2()*m1())+1/2*(2*k1()*m2()*u10()-k2()*m1()*u8()+2*k2()*m2()*u10())*x1^2/(m1()*m2())+1/2*(k1()*m2()*u8()-k2()*m1()*u8()+2*k2()*m2()*u10())*x2^2/(m1()*m2())>=0->[{x1'=v1,v1'=-k1()/m1()*x1-k2()/m1()*(x1-x2),x2'=v2,v2'=-k2()/m2()*(x2-x1)&true}](!u8()*v1*v2+k2()*x1*x2*(m1()*u8()-2*m2()*u10())/(m1()*m2())+u10()*v1^2+u1()+1/2*v2^2*(k1()*m2()*u8()-k2()*m1()*u8()+k2()*m2()*u8()+2*k2()*m2()*u10())/(k2()*m1())+1/2*(2*k1()*m2()*u10()-k2()*m1()*u8()+2*k2()*m2()*u10())*x1^2/(m1()*m2())+1/2*(k1()*m2()*u8()-k2()*m1()*u8()+2*k2()*m2()*u10())*x2^2/(m1()*m2()) < 0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "3D Lotka Volterra (I)".
Definitions.
  R invcluster(R,R,R,R,R) = ( ._3*._0+._3*._1+._3*._2+._4 ).
End.

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x^2-1=0&y^2-1=0&z^2-1=0->[{x'=x*y-x*z,y'=y*z-y*x,z'=z*x-z*y&true}](x!=0&y!=0&z!=0)
End.

Tactic "Scripted proof".
implyR(1) ; cut({`\exists u3 \exists u4 (u3^2+u4^2!=0 & invcluster(x,y,z,u3,u4)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,z,u3,u4)=0`}, 1) ; doall(ODE(1)) ; done
    ,
    hideR(1) ; QE
  )
End.

Tactic "Automated proof".
master
End.
End.

Theorem "3D Lotka Volterra (II)".
Definitions.
  R invcluster(R,R,R,R,R,R,R,R) = ( ._3*._0*._1*._2+._4*._0^3+3*._4*._0^2*._1+3*._4*._0^2*._2+3*._0*._1^2*._4+3*._0*._2^2*._4+._4*._1^3+3*._4*._1^2*._2+3*._4*._1*._2^2+._4*._2^3+._5*._0^2+2*._5*._0*._1+2*._5*._0*._2+._5*._1^2+2*._5*._1*._2+._5*._2^2+._6*._0+._6*._1+._6*._2+._7 ).
End.

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x^2-1=0&y^2-1=0&z^2-1=0->[{x'=x*y-x*z,y'=y*z-y*x,z'=z*x-z*y&true}](x!=0&y!=0&z!=0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Hybrid Controller Mode 1".
Definitions.
  R invcluster(R,R,R,R) = ( -1/5*._3*._0^2+1/10*._3*._1^2+8*._3*._0+._3*._1+._2 ).
End.

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (x-9)^2+(y-20)^20<=4->[{x'=y^2+10*y+25,y'=2*x*y+10*x-40*y-200&5<=x&x<=35}]y<=48
End.

Tactic "Manual proof".
implyR(1) ; cut({`\exists u11 \exists u12 (u11^2+u12^2!=0 & invcluster(x,y,u11,u12)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,u11,u12)=0`}, 1) ; <(
      dW(1); QE,
      ODE(1)
    )
    ,
    hideR(1) ; QE
  )
End.
End.

Theorem "Hybrid Controller Mode 2".
Definitions.
  R invcluster(R,R,R,R) = ( 4/5*._3*._0^2+1/10*._3*._1^2-32*._3*._0+._3*._1+._2 ).
End.

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (x-9)^2+(y-20)^20<=4->[{x'=-y^2-10*y-25,y'=8*x*y+40*x-160*y-800&5<=x&x<=35}]y<=48
End.

Tactic "Scripted proof".
implyR(1) ; cut({`\exists u21 \exists u22 (u21^2+u22^2!=0 & invcluster(x,y,u21,u22)=0)`}) ; <(
    existsL('Llast)*2 ; dC({`invcluster(x,y,u21,u22)=0`}, 1) ; <(
      dW(1); QE,
      dI(1)
    )
    ,
    hideR(1) ; QE
  )
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Harmonic Oscillator".
Definitions.
  R k().
End.

ProgramVariables.
  R x.
  R p.
End.

Problem.
  x^2+p^2=k()^2->[{x'=p,p'=-k()^2*x&true}]x^2+p^2=k()^2
End.


End.

Theorem "Rigid Body".
Definitions.
  R c1().
  R c2().
  R c3().
End.

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  true->[{x'=y*z/c3()-z*y/c2(),y'=-x*z/c3()+z*x/c1(),z'=x*y/c2()-y*x/c1()&true}]false
End.


End.

Theorem "3D Lotka Volterra (III)".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x^2-1=0&y^2-1=0&z^2-1=0->[{x'=x*y-x*z,y'=y*z-y*x,z'=z*x-z*y&true}](x!=0&y!=0&z!=0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Coupled Spring-Mass System (II)".
Definitions.
  R k().
End.

ProgramVariables.
  R x1.
  R v1.
  R x2.
  R v2.
End.

Problem.
  x1=0&x2=0&v1=1&v2=-1->[{x1'=v1,x2'=v2,v1'=-k()/5*(x1-x2),v2'=k()*(x1-x2)&true}]true
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Collision Avoidance Maneuver (I)".
Definitions.
  R theta().
  R r2().
  R w().
  R a().
  R r1().
  R b().
End.

ProgramVariables.
  R x1.
  R e1.
  R d1.
  R x2.
  R y1.
  R d2.
  R y2.
  R e2.
End.

Problem.
  x1=r1()&y1=r1()&x2=r2()&y2=r2()&d1=a()&d2=0&e1=b()&e2=0&(x1-y1)^2+(x2-y2)^2>0->[{x1'=d1,x2'=d2,d1'=-w()*d2,d2'=w()*d1,y1'=e1,y2'=e2,e1'=-theta()*e2,e2'=theta()*e1&true}](x1-y1)^2+(x2-y2)^2>0
End.


End.

Theorem "Constraint-based Example 7 (Human Blood Glucose Metabolism)".
Definitions.
  R u().
End.

ProgramVariables.
  R IG.
  R IPI.
  R IH.
  R IK.
  R IL.
  R IPV.
  R IB.
End.

Problem.
  20<=u()&u()<=25->[{IB'=-45*IB/26+45*IH/26,IH'=5*IB/11-104*IH/33+8*IK/11+10*IL/11+35*IPV/33+u(),IG'=-36*IG/47+36*IH/47,IL'=2376*IG/7375+204*IH/2375-45*IL/59,IK'=24*IH/17-3459*IK/2125,IPV'=105*IH/74+337*IPI/740-1387*IPV/740,IPI'=-2381*IPI/5158+IPV/20&true}]false
End.


End.

Theorem "Constraint-based Example 8 (Phytoplankton Growth)".

ProgramVariables.
  R x1.
  R x3.
  R x2.
End.

Problem.
  x1=0&x2=0&x3=0->[{x1'=1-x1-x1*x2/4,x2'=x2*(-1+2*x3),x3'=x1/4-2*x3^2&true}](x1<=5&x2<=5&x3<=5)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Constraint-based Example 9 (Disjunctive Invariants)".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>=3->[{x'=-y,y'=-x&true}](x>=0|y>=0)
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Collision Avoidance Maneuver (II)".
Definitions.
  R w2().
  R w1().
End.

ProgramVariables.
  R x1.
  R e1.
  R d1.
  R x2.
  R y1.
  R d2.
  R y2.
  R e2.
End.

Problem.
  (x1-y1)^2+(x2-y2)^2>0->[{x1'=d1,x2'=d2,d1'=-w1()*d2,d2'=w1()*d1,y1'=e1,y2'=e2,e1'=-w2()*e2,e2'=w2()*e1&true}](x1-y1)^2+(x2-y2)^2>0
End.


End.

Theorem "Collision Avoidance Maneuver (III)".
Definitions.
  R w2().
  R w1().
End.

ProgramVariables.
  R x1.
  R e1.
  R d1.
  R x2.
  R y1.
  R d2.
  R y2.
  R e2.
End.

Problem.
  w1()=w2()&(x1-y1)^2+(x2-y2)^2>0->[{x1'=d1,x2'=d2,d1'=-w1()*d2,d2'=w1()*d1,y1'=e1,y2'=e2,e1'=-w2()*e2,e2'=w2()*e1&true}](x1-y1)^2+(x2-y2)^2>0
End.


End.

Theorem "Longitudinal Motion of an Aircraft".
Definitions.
  R M().
  R Iyy().
  R m().
  R X().
  R g().
  R Z().
End.

ProgramVariables.
  R w.
  R theta.
  R q.
  R d1.
  R x.
  R d2.
  R u.
  R z.
End.

Problem.
  true->[{u'=X()/m()-g()*d1-q*w,x'=d2*u+d1*w,w'=Z()/m()+g()*d2+q*u,z'=-d1*u+d2*w,theta'=q,q'=M()/Iyy(),d1'=q*d2,d2'=-q*d1&true}]false
End.


End.

Theorem "Nonlinear Circuit Example 1+2 (Tunnel Diode Oscillator)".
Definitions.
  R L() = ( 0.000001 ).
  R G() = ( 2 ).
  R Id(R).
  R Vin() = ( 0.3 ).
  R C() = ( 0.000000001 ).
End.

ProgramVariables.
  R Il.
  R Vc.
End.

Problem.
  Vc=0.131&Il=0.055->[{Vc'=1/0.000000001*(-Id(Vc)+Il),Il'=1/0.000001*(-Vc-1/2*Il+0.3)&true}](-0.5<=Vc&Vc<=1.2&-0.5<=Il&Il<=0.2)
End.


End.

Theorem "Nonlinear Circuit Example 3".

ProgramVariables.
  R x1.
  R x2.
End.

Problem.
  x1>=2&x2^2<=1->[{x1'=-x2^3,x2'=x1-x1^3&true}]x1>=1
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Nonlinear Circuit Example 4".

ProgramVariables.
  R x1.
  R x2.
End.

Problem.
  -1.1<=x1&x1<=-0.7&0.5<=x2&x2<=0.9->[{x1'=-x2^3,x2'=x1-x1^3&true}]x1^2+x2-3 < 0
End.


End.

Theorem "Nonlinear Circuit RLC Circuit Oscillator".

ProgramVariables.
  R Il.
  R Vc.
End.

Problem.
  Il=1&Vc=1->[{Il'=-Vc-1/5*Vc^2,Vc'=-2*Il-Il^2+Il^3&true}]Vc<=3
End.

Tactic "Scripted proof".
implyR(1);
  dC({`1-5*Il^3-15*Il^2+Vc^3+15/2*Vc^2+15/4*Il^4 <= 0`}, 1); /* invariant from paper */
  <(ODE(1), ODE(1))
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Normalized Pendulum".
Definitions.
  R LyapunovCandidate(R,R) = ( 100*._0^2+24*._0*._1+24*._1*._0+92*._1^2 ).
End.

ProgramVariables.
  R x1.
  R x2.
  R cosx1.
  R sinx1.
End.

Problem.
  100*x1^2+24*x1*x2+24*x2*x1+92*x2^2 < 0&x1=1&sinx1=1&cosx1=0->[{x1'=x2,x2'=-sinx1-x2,sinx1'=cosx1,cosx1'=-sinx1&true}]100*x1^2+24*x1*x2+24*x2*x1+92*x2^2 < 0
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Damped Mathieu System".
Definitions.
  R LyapunovCandidate(R,R) = ( 98*._0^2+24*._0*._1+24*._1*._0+55*._1^2 ).
End.

ProgramVariables.
  R x1.
  R x2.
  R sint.
  R cost.
End.

Problem.
  98*x1^2+24*x1*x2+24*x2*x1+55*x2^2 < 0&sint=0&cost=1->[{x1'=x2,x2'=-x2-(2+sint)*x1,sint'=cost,cost'=-sint&true}]98*x1^2+24*x1*x2+24*x2*x1+55*x2^2 < 0
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Switched-Mode System".
Definitions.
  R g(R) = ( 0.1*e^.-0.1 ).
  R e() = ( 2.71828 ).
  R LyapunovCandidate(R,R) = ( 11*._0^2+1*._0*._1+1*._1*._0+100*._1^2 ).
End.

ProgramVariables.
  R x1.
  R x2.
End.

Problem.
  11*x1^2+1*x1*x2+1*x2*x1+100*x2^2 < 0->[{x1'=-0.1*x1+x2,x2'=-10*x1-0.1*x2&x1>=0&x2>=0.1*2.71828^x1-0.1|x1<=0&x2<=0.1*2.71828^x1-0.1}++{x1'=-0.1*x1+10*x2,x2'=-x1-0.1*x2&x1<=0&x2>=0.1*2.71828^x1-0.1|x1>=0&x2<=0.1*2.71828^x1-0.1}]11*x1^2+1*x1*x2+1*x2*x1+100*x2^2 < 0
End.

Tactic "Automated proof".
master
End.
End.

Theorem "Powertrain Control System".
Definitions.
  R c10() = ( -0.00063 ).
  R c16() = ( 1 ).
  R c8() = ( -0.05231 ).
  R c7() = ( 2.821 ).
  R fp(R) = ( c3+c4*c2*.+c5*c2*.^2+c6*c2^2*. ).
  R c14() = ( 0.4 ).
  R c11() = ( 1 ).
  R c12() = ( 14.7 ).
  R sqrt(R) = ( .^(1/2) ).
  R c4() = ( 0.08979 ).
  R c1() = ( 0.41328 ).
  R c15() = ( 0.4 ).
  R u1() = ( 23.0829 ).
  R c5() = ( -0.0337 ).
  R c13() = ( 0.9 ).
  B initial(R,R,R,R) <-> ( ._0^2+._1^2+._2^2+._3^2<=0.004 ).
  R c6() = ( 0.0001 ).
  R c9() = ( 0.10299 ).
  B unsafe(R) <-> ( .^2>0.01 ).
  R c2() = ( 200 ).
  R c3() = ( -0.366 ).
End.

ProgramVariables.
  R i.
  R r.
  R p.
  R pest.
End.

Problem.
  p^2+r^2+pest^2+i^2<=0.004->[{p'=0.41328*(2*23.0829*(p/1-(p/1)^2)^(1/2)-(-0.366+0.08979*200*p+-0.0337*200*p^2+0.0001*200^2*p)),r'=4*((-0.366+0.08979*200*p+-0.0337*200*p^2+0.0001*200^2*p)/(0.9*(-0.366+0.08979*200*pest+-0.0337*200*pest^2+0.0001*200^2*pest)*(1+i+0.4*(r-1)))-r),pest'=0.41328*(2*23.0829*(p/1-(p/1)^2)^(1/2)-0.9*(-0.366+0.08979*200*pest+-0.0337*200*pest^2+0.0001*200^2*pest)),i'=0.4*(r-1)&true}](!r^2>0.01)
End.


End.