/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 13/10/2020 */

#ifndef INPUT_HH_
#define INPUT_HH_

#include <float.h>

/* state space dim */
#define sDIM 4
#define iDIM 2

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;
typedef std::array<double,2> state_xy_dim_type;

/* parallel or sequential implementation of computation of the
 transition functions */
const bool parallel = false;
int verbose = 1; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
/* read transitions from file flag */
const bool RFF = true;
///* scaling */
//const double scale = 1;
/* sampling time */
const double tau = 0.08;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.06,0.06,0.0,0.0};
const double eps = 0.25; /* the radius of the reachable set from the state dimensions 3 and 4*/

/* lower bounds of the hyper rectangle */
double lb[sDIM]={0.0,0.0,-0.5,-0.5};
/* upper bounds of the hyper rectangle */
double ub[sDIM]={2.0,2.0,2.0,2.0};
/* grid node distance diameter */
double eta[sDIM]={0.08,0.08,1.0,1.0};

/* input space parameters */
double ilb[iDIM]={-1.0,-1.0};
double iub[iDIM]={1.0,1.0};
double ieta[iDIM]={0.1,0.1};

/* goal states */
/* add inner approximation of P={ x | H x<= h } form state space */
/* office */
double H1[6*sDIM]={-1, 0, 0, 0,
    1, 0, 0, 0,
    0,-1, 0, 0,
    0, 1, 0, 0,
    0, 0, 0, -1,
    0, 0, 0, 1
};
double g1[6] = {-0.3,0.7,-1.3,1.7,-0.49,1.51};
/* kitchen */
double H2[4*sDIM]={-1, 0, 0, 0,
    1, 0, 0, 0,
    0,-1, 0, 0,
    0, 1, 0, 0};
double g2[4] = {-1.3,1.7,-0.3,0.7};

/* static obstacle set */
double HO[4*sDIM]={-1, 0, 0, 0,
    1, 0, 0, 0,
    0,-1, 0, 0,
    0, 1, 0, 0};
//double ho1[4] = {0,1.1,-0.8,0.9};
double ho1[4] = {-1.11,1.19,-0.9,1.7};
double ho2[4] = {-1.1,1.2,-1.7,2.0};
/* dynamic obstacle: closed door */
double HDO[6*sDIM]={-1, 0, 0, 0,
    1, 0, 0, 0,
    0,-1, 0, 0,
    0, 1, 0, 0,
    0, 0, -1, 0,
    0, 0, 1, 0,
};
//double ho3[6] = {-1.11,1.19,-0.9,1.7,0.49,0.49};
double ho3[6] = {0,1.1,-0.8,0.9,0.49,0.49};
/* assumptions */
/* coffee appears */
double HC[2*sDIM]={0, 0, 0, -1,
    0, 0, 0, 1};
double a1[2] = {-0.51,1.49};
/* door opens */
double HD[2*sDIM]={0, 0, -1, 0,
    0, 0, 1, 0};
double a2[2] = {-0.51,1.49};

auto check_intersection = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_intersection=true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i=0; i<2; i++) {
        if ((ub2[i]-lb1[i]<DBL_MIN) || (lb2[i]-ub1[i]>DBL_MIN)) {
            is_intersection=false;
            break;
        }
    }
    return is_intersection;
};

auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
    state_type y;
    if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    }
//    /* update the flags */
//    if ((z1[2]!=z2[2]) | (z1[3]!=z2[3])) {
//        std::runtime_error("The two diagonal vertices of the cube should agree on the status of the environment flags.");
//    }
    /* the X-Y dimension of the corners of the current state */
    state_xy_dim_type z1_xy_dim, z2_xy_dim;
    z1_xy_dim[0]=z1[0];
    z1_xy_dim[1]=z1[1];
    z2_xy_dim[0]=z2[0];
    z2_xy_dim[1]=z2[1];
    if (z1[2]<=0.5 && z2[2]<=0.5) {
        /* if the door is closed, it may open or close in the next time step */
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set approximation */
            y[2] = 0.5-eps;
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            y[2] = 0.5+eps;
        }
    } else {
//        /* the hyperrectangle representing the location of the door */
//        state_xy_dim_type lb1, ub1;
//        lb1[0]=(std::lround(-ho3[0]/eta[0])-0.5)*eta[0];
//        lb1[1]=(std::lround(-ho3[2]/eta[1])-0.5)*eta[1];
//        ub1[0]=(std::lround(ho3[1]/eta[0])+0.5)*eta[0];
//        ub1[1]=(std::lround(ho3[3]/eta[1])+0.5)*eta[1];
        /* the hyperrectangle representing the location of the office */
        state_xy_dim_type lb1, ub1;
        lb1[0]=-g1[0];
        lb1[1]=-g1[2];
        ub1[0]=g1[1];
        ub1[1]=g1[3];
        /* the hyperrectangle representing the location of the kitchen */
        state_xy_dim_type lb2, ub2;
        lb2[0]=-g2[0];
        lb2[1]=-g2[2];
        ub2[0]=g2[1];
        ub2[1]=g2[3];
        /* when the door opens, it remains open until the robot reaches the kitchen */
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            if (check_intersection(lb2,ub2,z1_xy_dim,z2_xy_dim)) {
                y[2]=0.0-eps;
            } else {
                y[2]=1.0-eps;
            }
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            if ( check_intersection(lb2,ub2,z2_xy_dim,z1_xy_dim)) {
                y[2]=0.0+eps;
            } else {
                y[2]=1.0+eps;
            }
        }
//        /* when the door opens, it remains open until the robot passes through the door */
//        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
//            if (check_intersection(lb1,ub1,z1_xy_dim,z2_xy_dim)) {
//                y[2]=0.0-eps;
//            } else {
//                y[2]=1.0-eps;
//            }
//        } else { /* z1 is the upper bound and z2 is the lower bound */
//            /* return the upper right corner of the reachable set approximation */
//            if (check_intersection(lb1,ub1,z1_xy_dim,z2_xy_dim)) {
//                y[2]=0.0+eps;
//            } else {
//                y[2]=1.0+eps;
//            }
//        }
    }
    if (z1[3]<=0.5 && z2[3]<=0.5) {
        /* if there is no request, a request may appear any time */
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set approximation */
            y[3] = 0.5-eps;
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            y[3] = 0.5+eps;
        }
    } else {
        /* the hyperrectangle representing the location of the request */
        state_xy_dim_type lb1, ub1;
        lb1[0]=-g1[0];
        lb1[1]=-g1[2];
        ub1[0]=g1[1];
        ub1[1]=g1[3];
        /* when a request appears, it remains active until the request is served */
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set */
            if (check_intersection(lb1,ub1,z1_xy_dim,z2_xy_dim)) {
                y[3]=0-eps;
            } else {
                y[3]=1-eps;
            }
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            if (check_intersection(lb1,ub1,z2_xy_dim,z1_xy_dim)) {
                /* return the lower left corner of the hyper-rectangle */
                y[3]=0+eps;
            } else {
                y[3]=1+eps;
            }
        }
    }
    
    return y;
};

#endif /* INPUT_HH_ */
