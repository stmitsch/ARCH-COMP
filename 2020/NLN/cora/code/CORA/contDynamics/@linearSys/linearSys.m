classdef linearSys < contDynamics
% linearSys class (linSys: linear system)
%
% Syntax:  
%    object constructor: Obj = linearSys(varargin)
%                        Obj = linearSys(name,A,B)
%                        Obj = linearSys(name,A,B,c)
%                        Obj = linearSys(name,A,B,c,C)
%                        Obj = linearSys(name,A,B,c,C,D)
%                        Obj = linearSys(name,A,B,c,C,D,k)
%    copy constructor: Obj = otherObj
%
% Description:
%    Generates a linear system object according to the following first
%    order differential equations:
%       x' = A x + B u + c
%       y = C x + D u + k
%
% Inputs:
%    name - name of system
%    A - state matrix
%    B - input matrix
%    c - constant input
%    C - output matrix
%    D - throughput matrix
%    k - output offset
%
% Outputs:
%    Obj - Generated Object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      23-January-2007 
% Last update:  30-April-2007
%               04-August-2016 (changed to new OO format)
%               01-November-2017 (constant input added)
%               20-March-2018 (NK, output equation parameter added)
%               07-November-2018 (MA, default values for B and C changed)
%               04-March-2019 (MA, default IDs for inputs and outputs)
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    A = []; % system matrix
    B = 1; % input matrix
    c = []; % constant input
    C = 1; % output matrix
    D = []; % throughput matrix
    k = []; % output offset
    taylor = [];
    krylov = [];
end

methods
    %class constructor
    function obj = linearSys(varargin)
        % default IDs for states, inputs, and outputs
        stateIDs = ones(length(varargin{2}),1);
        inputIDs = 1; %default
        outputIDs = 1; %default
        if nargin>=3
            % default IDs for inputs
            B = varargin{3};
            if ~isscalar(B)
                inputIDs = ones(length(B(1,:)),1);
            else
                inputIDs = stateIDs;
            end
            if nargin >= 5
                C = varargin{5};
                % nr of outputs
                outputIDs = ones(length(C(:,1)),1);
            end
        end
        % instantiate parent class
        obj@contDynamics(varargin{1},stateIDs,inputIDs,outputIDs); 
        % 3 or more inputs
        if nargin>=3
            obj.A = varargin{2};
            obj.B = varargin{3};
            if nargin >= 4
                obj.c = varargin{4};
            end
            if nargin >= 5
                obj.C = varargin{5};
            end
            if nargin >= 6
                obj.D = varargin{6};
            end
            if nargin == 7
                obj.k = varargin{7}; 
            end
        end
    end
end
end

%------------- END OF CODE --------------