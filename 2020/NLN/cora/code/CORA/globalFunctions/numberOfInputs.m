function count = numberOfInputs(f,inpArgs)
% numberOfInputs - computes the number of inputs of a function handle
%
% Syntax:  
%    count = numberOfInputs(f,inpArgs)
%
% Inputs:
%    f - function handle
%    inpArgs - number of input arguments for the function 
%
% Outputs:
%    count - vector storing the length of each input argument
%
% Example:
%    f = @(x,u) [x(1)*x(5)^2; sin(x(3)) + u(2)];
%    numberOfInputs(f,2)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: nonlinearSys

% Author:       Niklas Kochdumper
% Written:      03-May-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    if inpArgs > 26
       error('That many input arguments are not supported!'); 
    end
    
    % get upper bound for the length of each input argument
    bound = 1000;
    input = cell(inpArgs,1);
    s = 'abcdefghijklmnopqrstuvwxyz';
    found = 0;
    
    for i = 1:3
       
        % ininialize input arguments with symbolic variables
        for j = 1:inpArgs
           input{j} = sym(s(j),[bound,1]);
        end
        
        % pass symbolic inputs to function handle
        try
            out{1} = f(input{:}); 
            found = 1;
            break;
        catch
            bound = bound * 10;
        end
    end
    
    if ~found
        error('Could not determine the length of the input arguments!');
    end
    
    % get all output arguments
    counter = 2;
    
    while true
       out_ = cell(counter,1);
       try
          [out_{:}] = f(input{:});
          out = out_;
          counter = counter + 1;
       catch
          break;
       end
    end
    
    % get all symbolic variables that are contained in the output
    vars = [];
    
    for i = 1:length(out)
       temp = symvar(out{i});
       vars = [vars,temp];
    end
    
    vars = unique(vars);
    
    % get required length for each input argument
    count = zeros(inpArgs,1);
    
    for i = 1:length(vars)
        
        % split variable into string and number
        temp = char(vars(i));
        str = temp(1);
        num = str2double(temp(2:end));
        
        % update counter
        ind = find(s == str);
        count(ind) = max(count(ind),num);
    end


%------------- END OF CODE --------------