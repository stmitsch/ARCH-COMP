function [pZ] = plus(summand1,summand2)
% plus - Overloaded '+' operator for the Minkowski addition of a
%        probabilistic zonotope with another summand according to Eq. 4 in
%        [1]
%
% Syntax:  
%    [pZ] = plus(summand1,summand2)
%
% Inputs:
%    summand1 - probabilistic zonotope object or other summand
%    summand2 - probabilistic zonotope object or other summand
%
% Outputs:
%    pZ - probabilistic Zonotpe 
%
% References:
%    [1] M. Althoff et al. "Safety assessment for stochastic linear systems 
%        using enclosing hulls of probability density functions", ECC 2009
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mtimes

% Author:       Matthias Althoff
% Written:      06-September-2007
% Last update:  24-August-2016
%               05-May-2020 (MW, standardized error message)
% Last revision:---

%------------- BEGIN CODE --------------

%Find a probabilistic zonotope object
%Is summand1 a zonotope?
if isa(summand1,'probZonotope')
    %initialize resulting zonotope
    pZ=summand1;
    %initialize other summand
    summand=summand2;
%Is summand2 a zonotope?    
elseif isa(summand2,'probZonotope')
    %initialize resulting zonotope
    pZ=summand2;
    %initialize other summand
    summand=summand1;  
end

%Is summand a probabilistic zonotope?
if isa(summand,'probZonotope')
    %Calculate minkowski sum
    pZ.Z=[pZ.Z(:,1)+summand.Z(:,1),pZ.Z(:,2:end),summand.Z(:,2:end)];
    %pZ.g=[pZ.g,summand.g];
    %pZ.sigma=[pZ.sigma,summand.sigma];
    pZ.cov=pZ.cov+summand.cov;

%Is summand a zonotope?
elseif isa(summand,'zonotope')
    %Calculate minkowski sum
    summandZ=get(summand,'Z');
    pZ.Z=[pZ.Z(:,1)+summandZ(:,1),pZ.Z(:,2:end),summandZ(:,2:end)];
    
%is summand a vector?
elseif isnumeric(summand)
    %Calculate minkowski sum
    pZ.Z(:,1)=pZ.Z(:,1)+summand;
    
%something else?    
else
    % throw error for given arguments
    error(noops(summand1,summand2));
end

%------------- END OF CODE --------------