function res = test_partition_get_zonotopes()
% test of the function segmentZonotope


%setup partitions
threeDimField=partition([0,10; -3,3; 0,1],[5;10;3]);
twoDimField=partition([0,10; -3,3],[5;10]);

% check that segmentZonotope works, 3DOF
Zons1 = segmentZonotope(twoDimField,0:twoDimField.totalSegments);
Zons2 = segmentZonotope(twoDimField);

if length(Zons1)==length(Zons2)
    res1 = (norm(center(Zons1{3}) - center(Zons2{3}))<1e-9)&&(norm(generators(Zons1{3}) - generators(Zons2{3}))<1e-9);
else
    res1 = 0;
end


% check that segmentZonotope works, 3DOF
Zons1 = segmentZonotope(threeDimField,0:threeDimField.totalSegments);
Zons2 = segmentZonotope(threeDimField);

if length(Zons1)==length(Zons2)
    res2 = (norm(center(Zons1{3}) - center(Zons2{3}))<1e-9)&&(norm(generators(Zons1{3}) - generators(Zons2{3}))<1e-9);
else
    res2 = 0;
end


res = res1&&res2;
% 
% segmentPolytope(threeDimField,[1 5 3])
% segmentPolytope(threeDimField)
% segmentZonotope(threeDimField,[1 5 3])
% segmentZonotope(threeDimField)
% P = mptPolytope([2 0 0.3;4 2 0.6;1 1 0.5; 1 1 0.1]);
% intersectingSegments(threeDimField,P)
% [iS,percentages] = exactIntersectingSegments(threeDimField,P)
% plot(threeDimField,exactIntersectingSegments(threeDimField,P))
% hold on
% plot(P)
% 
% %partition with 