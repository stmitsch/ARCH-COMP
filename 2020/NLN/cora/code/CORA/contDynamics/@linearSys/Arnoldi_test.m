function [V,H,Hlast,happyBreakdown] = Arnoldi_test(obj, varargin)

if nargin == 1
    v = rand(dim,1);
    redOrder = 50;
elseif nargin == 2
    v = varargin{1};
    redOrder = 50;
elseif nargin == 3
    v = varargin{1};
    redOrder = varargin{2};
end

[V,H,Hlast,happyBreakdown] = arnoldi(obj.A,v,redOrder);