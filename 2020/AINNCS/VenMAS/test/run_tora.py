#!/usr/bin/env python
from common import mono_verify

import keras
import datetime
import numpy as np
import math

from resources.tora.tora_agent import ToraAgent
from resources.tora.tora_env import ToraEnv

from src.network_parser.network_model import NetworkModel
from src.utils.formula import *
from src.verification.bounds.bounds import HyperRectangleBounds


def compute_trace():
    x1 = 0.6
    x2 = -0.7
    x3 = -0.4
    x4 = 0.5009584667319101

    print("state", 0, ":", "[{}, {}, {}, {}]".format(x1, x2, x3, x4))

    dt = 1

    offset = 10
    scaling_factor = 1

    controller = keras.models.load_model("../resources/tora/models/controllerTora_nnv.h5")
    steps = 1
    for i in range(steps):
        net_input = np.array([x1, x2, x3, x4]).reshape((1, 4))

        res = controller.predict(x=net_input, batch_size=1, verbose=0)
        [u] = res[0]

        u = (u - offset) * scaling_factor

        print("action", i, ":", [u])
        next_x1 = x1 + x2 * dt
        next_x2 = x2 - x1 * dt + 0.1 * math.sin(x3) * dt
        next_x3 = x3 + x4 * dt
        next_x4 = x4 + u * dt

        x1 = next_x1
        x2 = next_x2
        x3 = next_x3
        x4 = next_x4

        print("state", i+1, ":", "[{}, {}, {}, {}]".format(x1, x2, x3, x4))


def main():

    agent, env = initialise_and_get_agent_and_env()
    #
    # Constraint specific variables of the initial state to one value by setting the upper
    # bounds equal to the lower bounds.

    # hardcoding values here. assuming variable order [x1, ... , x4]
    unzipped = zip(*[(0.6, 0.7), (-0.7,-0.6), (-0.4,-0.3), (0.5,0.6)])
    input_hyper_rectangle = HyperRectangleBounds(*unzipped)

    # Safety leaf formula
    safe_x1_lower = VarConstConstraint(StateCoordinate(0), GT, -2.0)
    safe_x1_upper = VarConstConstraint(StateCoordinate(0), LT, 2.0)
    safe_x2_lower = VarConstConstraint(StateCoordinate(1), GT, -2.0)
    safe_x2_upper = VarConstConstraint(StateCoordinate(1), LT, 2.0)
    safe_x3_lower = VarConstConstraint(StateCoordinate(2), GT, -2.0)
    safe_x3_upper = VarConstConstraint(StateCoordinate(2), LT, 2.0)
    safe_x4_lower = VarConstConstraint(StateCoordinate(3), GT, -2.0)
    safe_x4_upper = VarConstConstraint(StateCoordinate(3), LT, 2.0)
    safe = NAryConjFormula([safe_x1_lower, safe_x1_upper, safe_x2_lower, safe_x2_upper, safe_x3_lower, safe_x3_upper, safe_x4_lower, safe_x4_upper])

    # we are required to check if controller always stays within interval for dt of 1s
    for num_steps in range(1,21):
        print(num_steps, "steps")

        # Specify formula encoding safety spec
        formula = ANextFormula(num_steps, safe)

        # Run verification method.
        # Monotlithic encoding
        log_info = mono_verify(formula, input_hyper_rectangle, agent, env)
        print("\n")

        with open("arch-comp.log", "a") as file:
            file.write(f"{datetime.datetime.now()}, Tora, {num_steps}, {log_info[0]:9.6f}, {log_info[1]}\n")


def initialise_and_get_agent_and_env():
    """
    Initialise agent and environment.
    :return: Initialised NeuralAgent and AccEnv objects.
    """

    sin_model = NetworkModel()
    sin_model.parse("../resources/tora/models/x_sin.h5")

    controller_model = NetworkModel()
    controller_model.parse("../resources/tora/models/controllerTora_nnv.h5")

    agent = ToraAgent(controller_model)
    env = ToraEnv(sin_model)

    return agent, env


if __name__ == "__main__":
    main()
    # compute_trace()