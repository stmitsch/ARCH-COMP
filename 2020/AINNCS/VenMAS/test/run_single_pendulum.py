#!/usr/bin/env python

import argparse
import math
import sys
sys.path.append('../')

import keras

import datetime

from resources.single_pendulum.pendulumenv import PendulumEnv
from src.actors.agents.neural_agent import NeuralAgent
from src.utils.constants import NeuralOutputSettings

from src.network_parser.network_model import NetworkModel
from src.utils.formula import *
from src.verification.bounds.bounds import HyperRectangleBounds

from common import mono_verify

import numpy as np

def compute_trace():
    theta = 1.200000000000000
    theta_dot = 0.05062117424131291
    print("state", 0, ":", "[{}, {}]".format(theta, theta_dot))

    dt = 0.05
    g = 1.0
    m = 0.5
    L = 0.5
    c = 0.

    controller = keras.models.load_model("../resources/single_pendulum/models/controller_single_pendulum.h5")
    steps = 10
    for i in range(steps):
        net_input = np.array([theta, theta_dot]).reshape((1,2))

        res = controller.predict(x=net_input, batch_size=1, verbose=0)
        torque = res[0][0]

        print("action", i, ":", torque)
        next_theta = theta + theta_dot * dt
        next_theta_dot = (1 - dt * c / (m * L**2)) * theta_dot + dt * g / L * math.sin(theta) + dt / (m * L**2) * torque

        theta = next_theta
        theta_dot = next_theta_dot

        print("state", i+1, ":", "[{}, {}]".format(theta, theta_dot))


def main():
    parser = argparse.ArgumentParser(description="Verify a NANES")
    parser.add_argument("-a", "--initial_theta", default=1.1, type=float, help="Initial angle (theta).")
    parser.add_argument("-v", "--initial_theta_dot", default=0.1, type=float, help="Initial angular velocity (theta dot).")
    parser.add_argument("-n", "--noise", default=0.1, type=float, help="Noise to add to initial angle and angular velocity.")
    parser.add_argument("-k1", "--min_steps", default=10, type=int, help="Minimum number of time steps to verify for.")
    parser.add_argument("-k2", "--max_steps", default=20, type=int, help="Maximum number of time steps to verify for.")
    parser.add_argument("-w", "--workers", default=2, type=int, help="Number of workers.")
    parser.add_argument("-t", "--timeout", default=3600, type=int, help="Timeout in seconds.")

    ARGS = parser.parse_args()

    agent, env = initialise_and_get_agent_and_env()

    # Constraint specific variables of the initial state to one value by setting the upper
    # bounds equal to the lower bounds.
    unzipped = zip(*[(ARGS.initial_theta - ARGS.noise,     ARGS.initial_theta + ARGS.noise),
                     (ARGS.initial_theta_dot - ARGS.noise, ARGS.initial_theta_dot + ARGS.noise)])

    input_hyper_rectangle = HyperRectangleBounds(*unzipped)
    print(input_hyper_rectangle)

    steps = range(ARGS.min_steps, ARGS.max_steps + 1)

    for num_steps in steps:
        print(num_steps, "steps")

        safe = AtomicConjFormula(VarConstConstraint(StateCoordinate(0), LT, 1.00001),
                                 VarConstConstraint(StateCoordinate(0), GT, 0.0))
        formula = ANextFormula(num_steps, safe)

        # Run a method.
        log_info = mono_verify(formula, input_hyper_rectangle, agent, env, ARGS.timeout)
        print("\n")

        with open("arch-comp.log", "a") as file:
            file.write(f"{datetime.datetime.now()}, Single Pendulum, {num_steps}, {log_info[0]:9.6f}, {log_info[1]}\n")


def initialise_and_get_agent_and_env():
    """
    Initialise agent and environment.
    :return: Initialised NeuralAgent and PendulumEnv objects.
    """

    x_sin_model = NetworkModel()
    x_sin_model.parse("../resources/single_pendulum/models/x_sin_0.0022.h5")

    controller_model = NetworkModel()
    controller_model.parse("../resources/single_pendulum/models/controller_single_pendulum.h5")

    agent = NeuralAgent(controller_model, output_setting=NeuralOutputSettings.Q_VALUES)
    env = PendulumEnv(x_sin_model)

    return agent, env


if __name__ == "__main__":
    main()
    # compute_trace()
