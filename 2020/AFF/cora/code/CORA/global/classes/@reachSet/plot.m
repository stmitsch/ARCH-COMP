function han = plot(R,varargin)
% plotFilled - plots a 2-dimensional projection of the reachable set
%
% Syntax:  
%    han = plot(R)
%    han = plot(R,dim,color)
%    han = plot(R,dim,color,'Order',order)
%    han = plot(R,dim,color,'Splits',splits)
%
% Inputs:
%    R - reachSet object
%    dim - dimensions that should be projected
%    color - color of the plotted set('r','b', etc.)
%    order - zonotope order for plotting (optional)
%    splits - number of splits for refinement of polyZonotopes (optional)
%
% Outputs:
%    han - handle for the resulting graphic object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plotFilled, reachSet

% Author:       Niklas Kochdumper
% Written:      02-June-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default values for the optional input arguments
dimensions = [1,2];
color = 'b';
order = [];
splits = [];

% parse input arguments
if nargin > 1 && ~isempty(varargin{1})
   dimensions = varargin{1}; 
end
if nargin > 2 && ~isempty(varargin{2})
   color = varargin{2};
end

type = {};
if nargin > 3
    type = varargin(3:end);
    for i = 1:2:length(type)-1
       if ischar(type{i})
           if strcmp(type{i},'Splits')
              splits = type{i+1};
              type{i} = [];
              type{i+1} = [];
           elseif strcmp(type{i},'Order')
              order = type{i+1};
              type{i} = [];
              type{i+1} = [];
           end
       end
    end
    type = type(~cellfun('isempty',type));
end

% loop over all reachable sets
hold on

for i = 1:size(R,1)
    
   % get reachable set
   if ~isempty(R(i,1).timeInterval)
       set = R(i,1).timeInterval.set; 
   else
       set = R(i,1).timePoint.set;
   end
    
   % loop over all time steps
   for j = 1:length(set)
       
       % project set to desired dimensions
       temp = project(set{j},dimensions);
       
       % order reduction
       if ~isempty(order)
           temp = reduce(temp,'girard',order);
       end
       
       % plot the set
       if isa(temp,'polyZonotope')
           if isempty(splits)
              han = plot(zonotope(temp),[1 2],color,type{:}); 
           else
              han = plot(temp,[1 2],color,'Splits',splits,type{:});  
           end
       else
           han = plot(temp,[1 2],color,type{:}); 
       end
   end
end

%------------- END OF CODE --------------