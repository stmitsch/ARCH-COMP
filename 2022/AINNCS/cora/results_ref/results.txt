--------------------------------------------------------
ARCH'22 AINNCS Category
Tool: CORA - A Tool for Continuous Reachability Analysis
Date: 25-Jul-2022 17:05:02
--------------------------------------------------------
 
Running 12 Benchmarks.. 
 
--------------------------------------------------------
BENCHMARK: Sherklock-Benchmark 10 (Unicycle Car Model)
Time to compute random Simulations: 1.0836
Time to check Violation in Simulations: 0.012185
...compute symbolic Jacobian
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
dynamics dim 6
Time to compute Reachable Set: 2.6859
Time to check Verification: 0.16118
Result: VERIFIED
Total Time: 3.9429
Plotting..
 
'01_unicycle' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Adaptive Cruise Controller (ACC)
Time to compute random Simulations: 0.94224
Time to check Violation in Simulations: 0.0030243
...compute symbolic Jacobian
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
dynamics dim 6
dynamics dim 7
Time to compute Reachable Set: 1.5869
Time to check Verification: 0.049175
Result: VERIFIED
Total Time: 2.5813
Plotting..
 
'02_ACC' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Sherlock-Benchmark-9 (TORA)
Time to compute random Simulations: 0.43501
Time to check Violation in Simulations: 0.0089707
...compute symbolic Jacobian
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
...compute symbolic third-order derivatives
dynamic index 1,1
dynamic index 1,2
dynamic index 1,3
dynamic index 1,4
dynamic index 1,5
dynamic index 1,6
dynamic index 2,1
dynamic index 2,2
dynamic index 2,3
dynamic index 2,4
dynamic index 2,5
dynamic index 2,6
dynamic index 3,1
dynamic index 3,2
dynamic index 3,3
dynamic index 3,4
dynamic index 3,5
dynamic index 3,6
dynamic index 4,1
dynamic index 4,2
dynamic index 4,3
dynamic index 4,4
dynamic index 4,5
dynamic index 4,6
dynamic index 5,1
dynamic index 5,2
dynamic index 5,3
dynamic index 5,4
dynamic index 5,5
dynamic index 5,6
Time to compute Reachable Set: 10.6547
Time to check Verification: 0.27649
Result: VERIFIED
Total Time: 11.3752
Plotting..
 
'03_TORA' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Single Pendulum
Time to compute random Simulations: 1.0298
Time to check Violation in Simulations: 0.0030485
Result: VIOLATED
Total Time: 1.0328
Plotting..
 
'04_singlePendulum' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (less robust)
Time to compute random Simulations: 0.7995
Time to check Violation in Simulations: 0.002714
Result: VIOLATED
Total Time: 0.80222
Plotting..
 
'05_doublePendulum_lessRobust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (more robust)
Time to compute random Simulations: 0.57492
Time to check Violation in Simulations: 0.0023562
Result: VIOLATED
Total Time: 0.57727
Plotting..
 
'05_doublePendulum_moreRobust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Airplane
Time to compute random Simulations: 1.457
Time to check Violation in Simulations: 0.0032885
Result: VIOLATED
Total Time: 1.4603
Plotting..
 
'06_airplane' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS_middle)
Time to compute random Simulations: 0.020078
Time to check Violation in Simulations: 0.0031103
Result: VIOLATED
Total Time: 0.023188
Plotting..
 
'07_VCAS_middle' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS_worst)
Time to compute random Simulations: 0.024666
Time to check Violation in Simulations: 0.0035907
Result: VIOLATED
Total Time: 0.028257
Plotting..
 
'07_VCAS_worst' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Attitude Control
Time to compute random Simulations: 0.55773
Time to check Violation in Simulations: 0.0053589
Time to compute Reachable Set: 1.0156
Time to check Verification: 0.011554
Result: VERIFIED
Total Time: 1.5902
Plotting..
 
'08_attitudeControl' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Quadrotor (QUAD)
Refined layer 3 from order 1 to 2!
Time to compute random Simulations: 0.81804
Time to check Violation in Simulations: 0.0043394

Abort analysis due to reachable set explosion!
  Step 8 at time t=2.5
The reachable sets until the current step are returned.

Time to compute Reachable Set: 1782.1507
Time to check Verification: 0.22133
Result: UNKOWN
Total Time: 1783.1944
Plotting..
 
'09_QUAD' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Spacecraft Docking
Time to compute random Simulations: 1.5695
Time to check Violation in Simulations: 0.0084425
Time to compute Reachable Set: 4.1904
Time to check Verification: 5.2892
Result: UNKOWN
Total Time: 11.0576
Plotting..
 
'10_spacecraftDocking' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
 
Completed!
