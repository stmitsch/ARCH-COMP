function [v_max, a_max] = conformantParameters(traj, s_0_delta, v_0_delta, t, delta_v, delta_a, v_start, a_start)
% conformantParameters - returns parameters v_max and a_max so that the
% data of the marker is reachset confrmant with the model.
% 
%
% Syntax:  
%    [v_max, a_max] = conformantParameters(traj, s_0_delta, v_0_delta, t, horizon)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    s_0_delta - radius of initial position uncertainty
%    v_0_delta - radius of initial velocity uncertainty
%    t - vector of points in time
%    
%
% Outputs:
%    v_max - maximum velocity
%    a_max - maximum acceleration
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------


% initial values
v_max = v_start;
a_max = a_start;

% init violation
violation = 1;

% set horizon to 0.2 seconds
horizon = 0.2;

% remaining time
t_rem = t;

% while no violation took place and current time is less than final time
while violation
    % obtain times when a violation is detected
    [t_violation_vel, t_violation_acc] = checkTrajectory(traj, s_0_delta, v_0_delta, v_max, a_max, t_rem, horizon);
    
    % check violation
    if t_violation_vel < inf 
        % increment v_max
        v_max = v_max + delta_v;
        % find corresponding index of violated time
        ind = find(t_violation_vel > t_rem, 1, 'last');
        % update remaining time
        t_rem(1:ind) = [];
    elseif t_violation_acc < inf
        % increment a_max
        a_max = a_max + delta_a;
        % find corresponding index of violated time
        ind = find(t_violation_acc > t_rem, 1, 'last');
        % update remaining time
        t_rem(1:ind) = [];
    else
        violation = 0;
    end
end


%------------- END OF CODE --------------