#!/bin/bash

echo "Entering run.sh"
../tools/VIBES-0.2.3/viewer/VIBes-viewer &
process_id=$!

#echo "Run PDS stage 3"
#(time ./pds_stage3) > results/pds_stage3-stats.txt 2>&1
#echo "Run PDS stage 2"
#(time ./pds_stage2) > results/pds_stage2-stats.txt 2>&1
#echo "Run PDS stage 1"
#(time ./pds_stage1) > results/pds_stage1-stats.txt 2>&1

echo "Run Robertson"
(time ./robertson) > results/rob-stats.txt 2>&1

echo "Run CVDP mu=1"
(time ./coupled_vdp-mu1-alex) >  results/cvdp-mu1-stats.txt 2>&1

echo "Run Laub Loomis Large"
(time ./laub-loomis-large) >  results/lll-stats.txt 2>&1
echo "Run Laub Loomis Mid"
(time ./laub-loomis-mid) >  results/llm-stats.txt 2>&1
echo "Run Laub Loomis Tight"
(time ./laub-loomis-tight) >  results/llt-stats.txt 2>&1

echo "Run Quadrotor 0.8"
(time ./quadcopter 8) >  results/q8-stats.txt 2>&1
echo "Run Quadrotor 0.4"
(time ./quadcopter 4) >  results/q4-stats.txt 2>&1
echo "Run Quadrotor 0.1"
(time ./quadcopter 1) >  results/q1-stats.txt 2>&1

echo "Run Hybrid Lotka-Volterra v2021"
(time ./lotka-volterra_2021) >  results/hlv-stats.txt 2>&1

echo "Run Space RDV v2021"
(time ./spacecraft_2021) >  results/srdv-stats.txt 2>&1

wait $process_id

#post treating
cp *.jpg results/

