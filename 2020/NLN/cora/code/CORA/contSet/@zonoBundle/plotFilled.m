function han = plotFilled(obj,varargin)
% plotFilled - plots 2-dimensional over-approximative projection of a zonotope bundle
%
% Syntax:  
%    han = plotFilled(obj,dim,plotOpt)
%
% Inputs:
%    obj - zonoBundle object
%    dim- dimensions that should be projected (optional) 
%    plotOpt - plot options (color, line width, etc.)
%
% Outputs:
%    han - handle for the graphics object
%
% Example: 
%    Z{1} = zonotope([0 1 1 -1;0 0 2 1]);
%    Z{2} = zonotope([2 1 1 -1;1 0 -1 -1]);
%    zB = zonoBundle(Z);
%
%    plot(zB,[1,2],'r','LineWidth',2);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plot

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      09-November-2010 
% Last update:  13-February-2012
%               19-October-2015 (NK, accelerate by using polyshape class)
% Last revision:---

%------------- BEGIN CODE --------------

% default settings
dim = [1,2];
plotOptions = {'b'};

% parse input arguments
if nargin >= 2 && ~isempty(varargin{1})
   dim = varargin{1}; 
end

if nargin >= 3
   plotOptions = varargin(2:end); 
end

w = warning;
warning('off','all');

% compute polytopes
for i = 1:obj.parallelSets
    
    % delete zero generators
    Z = deleteZeros(obj.Z{i});
    
    % project zonotope
    Z = project(Z,dim);
    
    % convert to polyshape (Matlab build in class)
    temp = polygon(Z);
    V = temp(:,2:end);
    P{i} = polyshape(V(1,:),V(2,:));
end

% intersect polytopes
Pint = P{1};
for i = 2:obj.parallelSets
    Pint = intersect(Pint,P{i});
end

% get vertices
V = Pint.Vertices;
V = V';

warning(w);
    
% Plot polytope
han = plotFilledPolygon(V,plotOptions{:});

%------------- END OF CODE --------------