Repeatability instructions KeYmaera X:

1. Run `setup.sh`, this will initialize the Docker container, install Wolfram Engine and Matlab, and build KeYmaera X. For example, on MacOS the Matlab license is in `/Users/{user-name}/Library/Application Support/MathWorks/MATLAB/R{XXXX}_licenses`
    ```
    sh setup.sh -l '/path/to/matlab.lic'
    ```
2. Run `measure_all.sh`, this starts the container and runs the benchmarks; after this step, the results are in the results/ subfolder. For example
    ```
    sh measure_all.sh
    ```