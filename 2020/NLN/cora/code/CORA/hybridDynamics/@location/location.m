function obj = location(name,id,invSet,trans,sys)
% location - constructor of class location
%
% Syntax:  
%    obj = location(name,id,invSet,trans,sys)
%
% Inputs:
%    name - name of the location: char array
%    id - identifier of the location (integer >= 1)
%    invSet - invariant set (class: contSet)
%    trans - cell-array containing all transitions transition
%    sys - continuous dynamics (class: contDynamics) 
%
% Outputs:
%    obj - Generated Object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: hybridAutomaton, transition

% Author: Matthias Althoff
% Written: 02-May-2007 
% Last update: ---
% Last revision: ---

%------------- BEGIN CODE --------------

    % store input arguments in object
    obj.name = name;
    obj.id = id;
    obj.transition = trans;
    obj.contDynamics = sys;
    
    % invariant set
    if isa(invSet,'mptPolytope') || isa(invSet,'levelSet')
        obj.invariant = invSet;
    else
        obj.invariant = mptPolytope(invSet);
    end

    % Register the variable as an object
    obj = class(obj, 'location'); 

%------------- END OF CODE --------------