function refinable_layers = getRefinableLayers(obj)
% getRefinableLayers - returns all layers which are refinable for
% adaptive evaluation
%
% Syntax:
%    res = getRefinableLayers(obj)
%
% Inputs:
%    obj - object of class NeuralNetwork
%
% Outputs:
%    None
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork/evaluate with 'adaptive'

% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

layers = obj.layers;
ix = arrayfun(@(layer_i) layer_i{1}.is_refinable, layers);
refinable_layers = layers(ix);

end
