function Hf=hessianTensorInt_car1(x,u)



 Hf{1} = interval(sparse(6,6),sparse(6,6));



 Hf{2} = interval(sparse(6,6),sparse(6,6));



 Hf{3} = interval(sparse(6,6),sparse(6,6));

Hf{3}(2,1) = -sin(x(2));
Hf{3}(1,2) = -sin(x(2));
Hf{3}(2,2) = -x(1)*cos(x(2));


 Hf{4} = interval(sparse(6,6),sparse(6,6));

Hf{4}(2,1) = cos(x(2));
Hf{4}(1,2) = cos(x(2));
Hf{4}(2,2) = -x(1)*sin(x(2));
