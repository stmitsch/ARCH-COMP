%
% simulate.m
%
% created on: 09.10.2015
%     author: rungger (vehicle.m)
%     modified by: kaushik
%
%
% you need to run ./vehicle binary first 
%
% so that the file: vehicle_controller_under.bdd is created
%

function simulate
    clear set
    close all
    addpath(genpath('../../../../mfiles'))

    %% simulation
    % initial state
    x0=[1.0,2.5,1.4];
    
    % real obstacle rectangle in the format [x y w h]
    avoid = [0.8, 1.0, 0.4, 0.4];

    % real target rectangle in the format [x y w h]
    target = [0.8, 0.2, 0.4, 0.4];

    % state space bounds in the form [xmin xmax ymin ymax]
    xlim = [0 2 0 3];

    % parameters for the simulation
    [~,~,W_ub,~,~,tau] = readParams('input.hh');
    V = 0.1;

    % prepare the figure window
    figure
    hold on
    axis(xlim);

    % load the symbolic set containing the controller
    controller=SymbolicSet('vehicle_controller_under.bdd');

    % plot the initial state, target, and obstacles
    plot(x0(1),x0(2),'k*');
    rectangle('Position',target,'FaceColor',[51, 204, 51]/255);
    rectangle('Position',avoid,'FaceColor','k');
    % label the initial state, target, and obstacles
    text(0.94,2.44,'$I$','interpreter','latex','FontSize',16);
    text(0.74,0.94,'$A$','interpreter','latex','FontSize',16);
    text(0.75,0.12,'$B$','interpreter','latex','FontSize',16);
    
    % simulate T time-steps
    y=x0;
    v=[];
    T=1000; 
    for t=1:T
        disp(t)
        u=controller.getInputs(y(end,:));

      v=[v; u(1,:)];
      x = vehicle(y(end,:),v(end,:)); 

      y=[y; x(end,:)];
      h1 = plot(y(end-1:end,1),y(end-1:end,2),'r.-');

       pause(0.05)
    end

    savefig('traj_stochastic');

    function xn = vehicle(x,u)
        if (x(3)>pi)
            x(3) = -pi + mod(x(3),pi);
        elseif (x(3)<-pi)
            x(3) = pi - mod(-x(3),pi);
        end
        % uniform random noise
        w = 2*W_ub.*(rand(1,3))' - W_ub;
        % worst case noise
    %         w = wmax;
        xn = zeros(size(x));
        if (u~=0)
            xn(1) = x(1) + V*sin(x(3)+u*tau)/u - V*sin(x(3))/u + w(1);
            xn(2) = x(2) - V*cos(x(3)+u*tau)/u + V*cos(x(3))/u + w(2);
            xn(3) = x(3) + u*tau + w(3);
        else
            xn(1) = x(1) + V*cos(x(3))*tau + w(1);
            xn(2) = x(2) - V*sin(x(3))*tau + w(2);
            xn(3) = x(3) + w(3);
        end
    end

end



