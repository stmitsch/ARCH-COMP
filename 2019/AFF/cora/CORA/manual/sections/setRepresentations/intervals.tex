A real-valued interval $[x]=[\underline{x},\overline{x}]=\{x \in \mathbb{R} | \underline{x} \leq x \leq \overline{x} \}$ is a connected subset of $\mathbb{R}$ and can be specified by a left bound $\underline{x}\in\mathbb{R}$ and right bound $\overline{x}\in\mathbb{R}$, where $\underline{x} \leq \overline{x}$. A detailed description of how intervals are treated in CORA can be found in \cite{Althoff2016a}. Since this class has a lot of methods, we separate them into methods that realize mathematical functions and methods that do not realize mathematical functions. 

\paragraph{Methods realizing mathematical functions and operations}
\begin{itemize}
 \item \texttt{abs} -- returns the absolute value as defined in \cite[Eq.~(10)]{Althoff2016a}. 
 \item \texttt{acos} -- $\arccos(\cdot)$ function as defined in \cite[Eq.~(6)]{Althoff2016a}. 
 \item \texttt{acosh} -- $\arccosh(\cdot)$ function as defined in \cite[Eq.~(8)]{Althoff2016a}. 
 \item \texttt{and} -- computes the intersection of two \texttt{intervals} as defined in \cite[Eq.~(1)]{Althoff2016a}.
 \item \texttt{asin} -- $\arcsin(\cdot)$ function as defined in \cite[Eq.~(6)]{Althoff2016a}. 
 \item \texttt{asinh} -- $\arcsinh(\cdot)$ function as defined in \cite[Eq.~(8)]{Althoff2016a}. 
 \item \texttt{atan} -- $\arctan(\cdot)$ function as defined in \cite[Eq.~(6)]{Althoff2016a}. 
 \item \texttt{atanh} -- $\arctanh(\cdot)$ function as defined in \cite[Eq.~(8)]{Althoff2016a}. 
 \item \texttt{cos} -- $\cos(\cdot)$ function as defined in \cite[Eq.~(13)]{Althoff2016a}. 
 \item \texttt{cosh} -- $\cosh(\cdot)$ function as defined in \cite[Eq.~(7)]{Althoff2016a}. 
 \item \texttt{ctranspose} -- overloaded '\,'\,' operator for single operand to transpose a matrix.
 \item \texttt{eq} -- overloads the '==' operator to check if both intervals are equal.
 \item \texttt{exp} -- exponential function as defined in \cite[Eq.~(4)]{Althoff2016a}. 
 \item \texttt{le} -- overloads \texttt{<=} operator: Is one interval equal or the subset of another interval? 
 \item \texttt{log} -- natural logarithm function as defined in \cite[Eq.~(5)]{Althoff2016a}. 
 \item \texttt{lt} -- overloads \texttt{<} operator: Is one interval equal or the subset of another interval?
 \item \texttt{minus} -- overloaded '-' operator, see \cite[Eq.~(2)]{Althoff2016a}. 
 \item \texttt{mpower} -- overloaded '\string^' operator (power), see \cite[Eq.~(9)]{Althoff2016a}. 
 \item \texttt{mrdivide} -- overloaded '/' operator (division), see \cite[Eq.~(3)]{Althoff2016a}. 
 \item \texttt{mtimes} -- overloaded '*' operator (multiplication), see \cite[Eq.~(2)]{Althoff2016a} for scalars and \cite[Eq.~(16)]{Althoff2016a} for matrices. 
 \item \texttt{ne} -- overloaded '~=' operator.
 %\item \texttt{or} -- computes union of intervals. Since intervals are not closed under unification, the result is an over-approximation. 
 \item \texttt{plus} -- overloaded '+' operator (addition), see \cite[Eq.~(2)]{Althoff2016a} for scalars and \cite[Eq.~(17)]{Althoff2016a} for matrices. 
 \item \texttt{power} -- overloaded '.\string^' operator for intervals (power), see \cite[Eq.~(9)]{Althoff2016a}. 
 \item \texttt{prod} -- product of array elements.
 \item \texttt{rdivide} -- overloads the './' operator: provides elementwise division of two matrices. 
 \item \texttt{sin} -- $\sin(\cdot)$ function as defined in \cite[Eq.~(12)]{Althoff2016a}. 
 \item \texttt{sinh} -- $\sinh(\cdot)$ function as defined in \cite[Eq.~(7)]{Althoff2016a}. 
 \item \texttt{sqrt} -- $\sqrt{(\cdot)}$ function as defined in \cite[Eq.~(5)]{Althoff2016a}. 
 \item \texttt{tan} -- $\tan(\cdot)$ function as defined in \cite[Eq.~(14)]{Althoff2016a}. 
 \item \texttt{tanh} -- $\tanh(\cdot)$ function as defined in \cite[Eq.~(7)]{Althoff2016a}. 
 \item \texttt{times} -- overloaded '.*' operator for elementwise multiplication of matrices. 
 \item \texttt{transpose} -- overloads the '\, .' \,' operator to compute the transpose of an interval matrix.
 \item \texttt{uminus} -- overloaded '-' operator for a single operand. 
 \item \texttt{uplus} -- overloaded '+' operator for single operand.  
\end{itemize}


\paragraph{Other methods}

\begin{itemize}
 \item \texttt{diag} -- create diagonal matrix or get diagonal elements of matrix.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclosingRadius} -- computes radius of enclosing hyperball of an interval.
 \item \texttt{enlarge} -- enlarges an \texttt{interval} object around its center.
 \item \texttt{gridPoints} -- computes grid points of an interval; the points are generated in a way such that a continuous space is uniformly partitioned.
 \item \texttt{horzcat} -- overloads the operator for horizontal concatenation, e.g., \texttt{a = [b,c,d]}.
 \item \texttt{hull} -- returns the union of two intervals.
 \item \texttt{in} -- determines if elements of a zonotope are in an interval.
 \item \texttt{infimum} -- returns the infimum of an interval.
 \item \texttt{interval} -- constructor of the class.
 \item \texttt{isempty} -- returns $1$ if an interval is empty and $0$ otherwise.
 \item \texttt{isIntersecting} -- determines if a set intersects an interval.
 \item \texttt{isscalar} -- returns $1$ if interval is scalar and $0$ otherwise. 
 \item \texttt{length} -- overloads the operator that returns the length of the longest array dimension.
 \item \texttt{mid} -- returns the center of an interval.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}. 
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{polytope} -- converts an interval object to a polytope.
 \item \texttt{rad} -- returns the radius of an interval.
 \item \texttt{reshape} -- overloads the operator 'reshape' for reshaping matrices.
 \item \texttt{size} -- overloads the operator that returns the size of the object, i.e., length of an array in each dimension.
 \item \texttt{subsasgn} -- overloads the operator that assigns elements of an interval matrix \texttt{I}, e.g., \texttt{I(1,2)=value}, where the element of the first row and second column is set.
 \item \texttt{subsref} -- overloads the operator that selects elements of an interval matrix \texttt{I}, e.g., \texttt{value=I(1,2)}, where the element of the first row and second column is read.
 \item \texttt{sum} -- overloaded 'sum()' operator for intervals.
 \item \texttt{supremum} -- returns the supremum of an interval. 
 \item \texttt{vertcat} -- overloads the operator for vertical concatenation, e.g., \texttt{a = [b;c;d]}.
 \item \texttt{vertices} -- returns a \texttt{vertices} object including all vertices. 
 \item \texttt{volume} -- computes the volume of an interval.
 \item \texttt{zonotope} -- converts an \texttt{interval} object to a \texttt{zonotope} object.
\end{itemize}



\subsubsection{Interval Example} \label{sec:intervalExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_interval.tex}}

This produces the workspace output
\begin{verbatim}
r =

     1.5000
     1.0000


is_intersecting =

     1


c =

    0.5000
   -0.7500
\end{verbatim}

The plot generated in lines 11-14 is shown in Fig. \ref{fig:intervalExample}.

\begin{figure}[h!tb]
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=0.8\columnwidth]{./figures/setRepresentationTest/intervalTest_1.eps}
  \caption{Sets generated in lines 11-14 of the interval example in Sec. \ref{sec:intervalExample}.}
  \label{fig:intervalExample}
\end{figure}