FROM ubuntu:18.04

MAINTAINER enea.zaffanella@unipr.it

RUN apt-get -y update
RUN apt-get -y install apt-utils software-properties-common wget
RUN apt-get -y install g++ make autoconf automake libtool
RUN apt-get -y install bison flex libgmp-dev libmpfr-dev libflint-dev

WORKDIR /

# Download PPLite 0.6 sources
RUN wget http://studenti.cs.unipr.it/~zaffanella/PPLite/releases/pplite-0.6.tar.gz
RUN tar zxf pplite-0.6.tar.gz

# Download PHAVerLite 0.3.1 sources
RUN wget http://studenti.cs.unipr.it/~zaffanella/PPLite/releases/phaverlite-0.3.1.tar.gz
RUN tar zxf phaverlite-0.3.1.tar.gz

# Download arch-comp20 models and config files
RUN wget http://studenti.cs.unipr.it/~zaffanella/PPLite/releases/arch-comp20.tar.gz
RUN tar zxf arch-comp20.tar.gz

# Build and install PPLite 0.6
RUN mkdir -p /build/pplite
WORKDIR /build/pplite
RUN ../../pplite-0.6/configure --enable-optimization=sspeed
RUN make -j2 && make install
WORKDIR /

# Build and install PHAVerLite 0.3.1
RUN mkdir -p /build/phaverlite
WORKDIR /build/phaverlite
RUN ../../phaverlite-0.3.1/configure --disable-assertions --disable-debugging --enable-optimization=sspeed --with-cxxflags='-s'
RUN make -j2
WORKDIR /

# Create symbolic link to stand-alone executable
RUN ln -s /build/phaverlite/src/phaverlite_static phaverlite-0.3.1_static

RUN mkdir -p /result

# Run all tests and produce /result/results.csv file
# RUN ./measure_all
