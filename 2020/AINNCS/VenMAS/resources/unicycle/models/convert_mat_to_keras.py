from keras.models import Sequential
from keras.layers import Dense

import numpy as np

from scipy.io import loadmat

# load weights and bias from .mat

W = loadmat('controllerB_nnv.mat')

# create "empty" Keras net to be filled with .mat

model = Sequential()
model.add(Dense(500, input_dim=4, activation='relu'))
model.add(Dense(2, activation='linear'))

# traverse the net layer by layer and set weights using the values extracted
# from the .mat

# extracting the .mat returns a python dictionary that can be easily accessed.

for index, l in enumerate(model.layers):
    l.set_weights([W['W'][0][index].transpose(),W['b'][0][index].flatten()])

# save the model into .h5 format

model.save('controllerB_nnv.h5')
