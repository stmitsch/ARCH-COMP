#/bin/sh
 
cd environment && docker build . --tag 930b4fcf-1366-444a-843e-7af86ea645bf; cd ..

docker run --rm --workdir /code --mac-address=8c:16:45:19:6c:93 --volume "$PWD/license.lic":/MATLAB/licenses/network.lic --volume "$PWD/data":/data --volume "$PWD/code":/code --volume "$PWD/results":/results 930b4fcf-1366-444a-843e-7af86ea645bf bash run
