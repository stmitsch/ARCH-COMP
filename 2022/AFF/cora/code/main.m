function main()

    addpath(genpath('../code'));

    % Install MP Toolbox ------------------------------------------------------------
    
    cd MultiPrecisionToolbox/@mp/private
    
    mp_compile_all
    
    cd ../../..


    % Install MPT Toolbox ------------------------------------------------------------

    % create installation directory
    mkdir('tbxmanager')
    cd tbxmanager

    % install toolbox-manager
    urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');
    tbxmanager
    savepath

    % install mpt-toolbox
    tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso

    % initialize toolbox
    mpt_init
    cd ..

    
    % initialize .csv file and track command-line outputs
    fid = fopen('../results/results.csv','w');
    diary '../results/computationTime'


    % HEAT 3D Benchmark -------------------------------------------------------------

    disp('Heat 3D -------------------------------------------------------------------');
    
    disp(' ');
    disp('HEAT01 ------------------');
    text = example_linear_reach_ARCH22_heat3D_HEAT01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT01.png');
    close;
    
    disp(' ');
    disp('HEAT02 ------------------');
    text = example_linear_reach_ARCH22_heat3D_HEAT02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT02.png');
    close;
    
    disp(' ');
    disp('HEAT03 ------------------');
    text = example_linear_reach_ARCH22_heat3D_HEAT03();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT03.png');
    close;

    
    % Beam Benchmark ----------------------------------------------------------------

    disp('Beam ----------------------------------------------------------------------');
    
    disp(' ');
    disp('CB01 ------------------');
    text = example_linear_reach_ARCH22_beam_CB01();
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    saveas(gcf, '../results/CB01_F.png');
    close(gcf);
    saveas(gcf, '../results/CB01_C.png');
    close(gcf);
    
    % ISS Benchmark -----------------------------------------------------------------

    disp('ISS -----------------------------------------------------------------------');
 
    disp(' ');
    disp('ISSC01_ISS02 ------------------');
    text = example_linear_reach_ARCH22_iss_ISSC01_ISS02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSC01_ISS02.png');
    close(gcf);

    disp(' ');
    disp('ISSC01_ISU02 ------------------');
    text = example_linear_reach_ARCH22_iss_ISSC01_ISU02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSC01_ISU02.png');
    close(gcf);

    disp(' ');
    disp('ISSF01_ISS01 ------------------');
    text = example_linear_reach_ARCH22_iss_ISSF01_ISS01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSF01_ISS01.png');
    close(gcf);

    disp(' ');
    disp('ISSF01_ISU01 ------------------');
    text = example_linear_reach_ARCH22_iss_ISSF01_ISU01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSF01_ISU01.png');
    close(gcf);

    % Building Benchmark ------------------------------------------------------------

    disp('Building ------------------------------------------------------------------');

    disp(' ');
    disp('BLDC01 ------------------');
    text = example_linear_reach_ARCH22_building_BLDC01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BLDC01.png');
    close(gcf);
    saveas(gcf, '../results/BLDC01_t1.png');
    close(gcf);

    disp(' ');
    disp('BLDF01 ------------------');
    text = example_linear_reach_ARCH22_building_BLDF01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BLDF01.png');
    close(gcf);
    saveas(gcf, '../results/BLDF01_t1.png');
    close(gcf);

    % Platoon ------------------------------------------------------------------------

    disp('Platoon --------------------------------------------------------------------');

    disp(' ');
    disp('PLAA01-BND42 ------------------');
    text = example_linearParam_reach_ARCH22_platoon_PLAA01_BND42;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAA01-BND42.png');
    close(gcf);

    disp(' ');
    disp('PLAA01-BND50 ------------------');
    text = example_linearParam_reach_ARCH22_platoon_PLAA01_BND50;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAA01-BND50.png');
    close(gcf);

    disp(' ');
    disp('PLAD01-BND30 ------------------');
    text = example_linearParam_reach_ARCH22_platoon_PLAD01_BND30;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAD01-BND30.png');
    close(gcf);

    disp(' ');
    disp('PLAD01-BND42 ------------------');
    text = example_linearParam_reach_ARCH22_platoon_PLAD01_BND42;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAD01-BND42.png');
    close(gcf);

    disp(' ');
    disp('PLAN01 ------------------');
    text = example_linearParam_reach_ARCH22_platoon_PLAN01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAN01.png');
    close(gcf);

    % Spacecraft Benchmark -----------------------------------------------------------

    disp('Spacecraft -----------------------------------------------------------------');

    disp(' ');
    disp('SRA01 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA01.png');
    close(gcf);

    disp(' ');
    disp('SRA02 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA02.png');
    close(gcf);

    disp(' ');
    disp('SRA03 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA03;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA03.png');
    close(gcf);

    disp(' ');
    disp('SRA04 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA04;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA04.png');
    close(gcf);

    disp(' ');
    disp('SRA05 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA05;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA05.png');
    close(gcf);

    disp(' ');
    disp('SRA06 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA06;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA06.png');
    close(gcf);

    disp(' ');
    disp('SRA07 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA07;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA07.png');
    close(gcf);

    disp(' ');
    disp('SRA08 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRA08;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA08.png');
    close(gcf);

    disp(' ');
    disp('SRNA01 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRNA01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRNA01.png');
    close(gcf);

    disp(' ');
    disp('SRU01 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRU01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRU01.png');
    close(gcf);

    disp(' ');
    disp('SRU02 ------------------');
    text = example_hybrid_reach_ARCH22_rendezvous_SRU02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRU02.png');
    close(gcf);

    % Powertrain Benchmark -----------------------------------------------------------

    disp('Powertrain -----------------------------------------------------------------');

    disp(' ');
    disp('DTN01 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN01.png');
    close(gcf);

    disp(' ');
    disp('DTN02 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN02.png');
    close(gcf);

    disp(' ');
    disp('DTN03 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN03;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN03.png');
    close(gcf);

    disp(' ');
    disp('DTN04 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN04;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN04.png');
    close(gcf);

    disp(' ');
    disp('DTN05 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN05;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN05.png');
    close(gcf);

    disp(' ');
    disp('DTN06 ------------------');
    text = example_hybrid_reach_ARCH22_powerTrain_DTN06;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTN06.png');
    close(gcf);

    % Gearbox Benchmark --------------------------------------------------------------

    disp('Gearbox --------------------------------------------------------------------');

    disp(' ');
    disp('GRBX01 ------------------');
    text = example_hybrid_reach_ARCH22_gearbox_GRBX01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/GRBX01.png');
    close(gcf);
    
    disp(' ');
    disp('GRBX02 ------------------');
    text = example_hybrid_reach_ARCH22_gearbox_GRBX02;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/GRBX02.png');
    close(gcf);

    % Brake Benchmark ----------------------------------------------------------------

    disp('Brake ---------------------------------------------------------------------');

    disp(' ');
    disp('BRKDC01 ------------------');
    text = example_hybrid_reach_ARCH22_brake_BRKDC01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKDC01.png');
    close(gcf);

    disp(' ');
    disp('BRKNC01 ------------------');
    text = example_hybrid_reach_ARCH22_brake_BRKNC01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKNC01.png');
    close(gcf);

    disp(' ');
    disp('BRKNP01 ------------------');
    text = example_hybrid_reach_ARCH22_brake_BRKNP01;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKNP01.png');
    close(gcf);


    % End ----------------------------------------------------------------------------

    % Close .csv file
    fclose(fid);

end