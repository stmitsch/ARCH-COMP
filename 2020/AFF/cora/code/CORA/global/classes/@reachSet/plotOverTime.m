function han = plotOverTime(R,varargin)
% plotOverTime - plots the reachable set over time
%
% Syntax:  
%    han = plotOverTime(R)
%    han = plotOverTime(R,dim,color)
%
% Inputs:
%    R - reachSet object
%    dim - dimension that should be projected
%    color - color of the plotted set('r','b', etc.)
%
% Outputs:
%    han - handle for the resulting graphic object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plot, plotFilled, reachSet

% Author:       Niklas Kochdumper
% Written:      02-June-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default values for the optional input arguments
dim = 1;
color = 'b';

% parse input arguments
if nargin > 1 && ~isempty(varargin{1})
   dim = varargin{1}; 
end
if nargin > 2 && ~isempty(varargin{2})
   color = varargin{2};
end

type = varargin(3:end);

% loop over all reachable sets
for i = 1:size(R,1)
   for j = 1:length(R(i,1).timeInterval.set)
       
       % get intervals
       intX = interval(project(R(i,1).timeInterval.set{j},dim));
       intT = R(i,1).timeInterval.time{j};
       
       int = cartProd(intT,intX);
       
       % plot interval
       han = plotFilled(int,[1,2],color,type{:});
   end
end

%------------- END OF CODE --------------