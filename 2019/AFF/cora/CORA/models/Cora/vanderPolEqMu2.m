function [dx]=vanderPolEqMu2(t,x,u)

    mu=2;

    dx(1,1)=x(2);
    dx(2,1)=mu*(1-x(1)^2)*x(2)-x(1)+u(1);
    
end