function f=parametricDynamicFile(x,u)

f{1}(1,1)=0;
f{1}(2,1)=u(1) - x(1);
f{2}(1,1)=x(2);
f{2}(2,1)=0;
f{3}(1,1)=0;
f{3}(2,1)=-x(2)*(x(1)^2 - 1);
