function [A,B]=jacobian_pendulum(x,u)

A=[0,1;...
-(981*cos(x(1)))/20,-1/2];

B=[0;...
0];

