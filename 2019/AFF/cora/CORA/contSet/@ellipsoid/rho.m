function [S] = rho(E,l)
% rho - Computes the support function of an ellipsoids object for
% directions l
%
% Syntax:  
%    S = rho(E,l) computes the support function of E for array of
%    directions l
% Inputs:
%    E - ellipsoids object
%    l - directions
%
% Outputs:
%    S - value of support function for each direction contained in l
%
% Example: 
%    t = linspace(0,2*pi,1000);
%    l = [cos(t);sin(t)];
%    E = ellipsoid([1,0;0,1/2],[1;1]);
%    S = rho(E,l);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      27-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
S = zeros(size(l,2),1);
for i=1:size(l,2)
    S(i) = l(:,i)'*E.q + sqrt(l(:,i)'*E.Q*l(:,i));
end


%------------- END OF CODE --------------