function P = polytope(zB,varargin)
% polytope - Converts a zonotope bundle to a polytope representation in an
%    exact way
%
% Syntax:  
%    P = polytope(zB)
%
% Inputs:
%    zB - zonoBundle object
%    options - options for enclosure
%
% Outputs:
%    P - polytope object
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff
% Written:      18-November-2010
% Last update:  30-July-2016
% Last revision:---

%------------- BEGIN CODE --------------

% compute over-approximative polytope for each zonotope
Ptmp = cell(zB.parallelSets,1);
for i=1:zB.parallelSets
    Ptmp{i} = polytope(zB.Z{i},varargin{1:end});
end

% intersect all polytopes
P = Ptmp{1};
for i=2:zB.parallelSets
    P = P & Ptmp{i};
end


%------------- END OF CODE --------------