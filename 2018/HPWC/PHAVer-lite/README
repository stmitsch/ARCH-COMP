This is the material related to the tool PHAVer-lite,
which took part to the friendly competition ARCH-COMP 2018
in the HPWC section (Hybrid Systems with Piecewise Constant Dynamics).

PHAVer-lite provides a variant of the PHAVer scenario from SpaceEx,
where the Parma Polyhedra Library (PPL) is replaced by the newly
developed library PPLite (http://www.cs.unipr.it/~zaffanella/PPLite).

The tool executable and libraries have been built on a laptop equipped with:
 - Intel Core i7-3632QM CPU @ 2.20GHz
 - 15.5 GB of RAM
running GNU/Linux 4.13.0-46 (Ubuntu 17.10, x86_64).

Binaries (sspaceex and memtime) and libraries are provided in subdirs
 * bin
 * lib

The model files for the 2018 benchmarks are contained in subdirs
 * ACC
 * DISC
 * FISC
 * DRNW
 * TTE

A couple of scripts are provided to run the tests:
 * run-ex-light.sh
   Runs the lighter tests, taking less than 6 mins overall
 * run-ex-hevay.sh
   Runs the two heavier tests, taking ~10 mins and ~4.1 hours

These scripts will produced output files:
  tests-light.out
  tests-light.out.filtered
  tests-heavy.out
  tests-heavy.out.filtered

For convenience, the output files produced on the machine said above
have been placed in directory results.
