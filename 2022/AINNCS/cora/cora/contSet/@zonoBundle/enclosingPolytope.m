function P = enclosingPolytope(zB,varargin)
% enclosingPolytope - encloses a zonotope bundle by a polytope
%
% Syntax:  
%    P = enclosingPolytope(zB,options)
%
% Inputs:
%    zB - zonoBundle object
%    options - options containing method of enclosure
%
% Outputs:
%    P - polytope object
%
% Example: 
%    ---
%
% Other m-files required: zonotope/enclosingPolytope
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff
% Written:      10-November-2010
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% compute over-approximative polytope for each zonotope
Ptmp = cell(zB.parallelSets,1);
for i=1:zB.parallelSets
    Ptmp{i} = enclosingPolytope(zB.Z{i},varargin{2:end});
end

% intersect all polytopes
P = Ptmp{1};
for i=2:zB.parallelSets
    P = P & Ptmp{i};
end

%------------- END OF CODE --------------