classdef neurNetContrSys < contDynamics
% neurNetContrSys - class that stores neural network controlled systems
%
% Syntax:  
%    obj = neurNetContrSys(sysOL,nn,dt)
%
% Inputs:
%    sysOL - dynamics of the uncontrolled system (class: contDynamics)
%    nn - neural network controller (class: neuralNetwork)
%    dt - sampling time
%
% Outputs:
%    obj - generated object
%
% Example:
%    % open-loop system
%    f = @(x,u) [x(2) + u(2); (1-x(1)^2)*x(2) - x(1) + u(1)];
%    sysOL = nonlinearSys(f);
%
%    % neural network controller
%    W{1} = rand(100,2); b{1} = rand(100,1);
%    W{2} = rand(1,100); b{2} = rand(1,1);
%    nn = neuralNetwork(W,b,'ReLU');
%
%    % neural network controlled system
%    dt = 0.01;
%    sys = neurNetContrSys(sysOL,nn,dt);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neurNetContrSys

% Author:       Niklas Kochdumper
% Written:      17-September-2021             
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    
    sys contDynamics;                           % system dynamics
    nn neuralNetwork;                           % neural network controller 
    dt (:,1) {mustBeNumeric,mustBeFinite, ...
              mustBeNonnegative} = 0;           % sampling time
end
    
methods
    
    % class constructor
    function obj = neurNetContrSys(sys,nn,dt)
       
       % check user input 
       n = sys.dim; m = nn.nrOfOutputs; w = sys.nrOfInputs;
       
       if w < m
           error(['Dimensions of open-loop system and neural network', ...
                                                   'are not consistent!']);
       end
       
       if nn.nrOfInputs ~= n
          error('System dynamics and neural network are not consistent!'); 
       end
        
       % construct closed-loop system
       if isa(sys,'nonlinearSys')  
           
          f = @(x,u) [sys.mFile(x(1:n),[x(n+1:n+m);u]);zeros(m,1)];
          name = [sys.name,'Controlled'];
          sys = nonlinearSys(name,f,n+m,max(1,sys.nrOfInputs-m));
          
       elseif isa(sys,'nonlinParamSys')
           
          f = @(x,u,p) [sys.mFile(x(1:n),[x(n+1:n+m);u],p);zeros(m,1)];
          name = [sys.name,'Controlled'];
          sys = nonlinParamSys(name,f,n+m,max(1,sys.nrOfInputs-m));
          
       else
          error('This type of system is not supported yet!'); 
       end
       
       % generate parent object
       obj@contDynamics(sys.name,n,max(1,sys.nrOfInputs-m),1);
       
       % assign object properties
       obj.sys = sys; obj.nn = nn; obj.dt = dt;
    end   
end
end

%------------- END OF CODE --------------