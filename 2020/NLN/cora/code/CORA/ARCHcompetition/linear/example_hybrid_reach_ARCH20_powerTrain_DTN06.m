function completed = example_hybrid_reach_ARCH20_powerTrain_DTN06()
% example_hybrid_reach_ARCH20_powerTrain_DTN06 - powertrain example 
%                                                (see [1]) from the 2020
%                                                ARCH competition
%                                                (Test case DTN06)
% 
%
% Syntax:  
%    example_hybrid_reach_ARCH20_powerTrain_DTN06()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% References:
%   [1] M. Althoff and B. H. Krogh. "Avoiding geometric intersection 
%       operations in reachability analysis of hybrid systems. HSCC 2012.
% 
% Author:       Matthias Althof, Niklas Kochdumper
% Written:      21-September-2011
% Last update:  12-March-2019
% Last revision:---


%------------- BEGIN CODE --------------


% System Dynamics ---------------------------------------------------------

% get system dynmics
[HA, Zcenter, Zdelta, B1, B2, B3, c1, c2, c3] = initPowerTrain(51); 

% split initial set
R01 = zonotope(Zcenter + 0.75*Zdelta,0.25*Zdelta);
R02 = zonotope(Zcenter + 0.25*Zdelta,0.25*Zdelta);
R03 = zonotope(Zcenter - 0.25*Zdelta,0.25*Zdelta);
R04 = zonotope(Zcenter - 0.75*Zdelta,0.25*Zdelta);


% Parameter  --------------------------------------------------------------

% initial set, initial location, and final time
params.startLoc = 3;
params.tFinal = 0.2; 

% system input
u = [-5; 0];            % acceleration and load torque

params.U{1} = B1*zonotope(0) + B1*u + c1;
params.U{2} = B2*zonotope(0) + B2*u + c2; 
params.U{3} = B3*zonotope(0) + B3*u + c3;



% Reachability Settings ---------------------------------------------------

% continuous reachability
options.taylorTerms = 20;
options.zonotopeOrder = 20;

options.timeStepLoc{1} = 2e-4;
options.timeStepLoc{2} = 1e-4;
options.timeStepLoc{3} = 1e-4;

% intersection with guard sets
options.guardIntersect = 'zonoGirard';
options.enclose = {'pca'};



% Simulation 1 ------------------------------------------------------------

% simulation parameter
paramsSim = params;
paramsSim = rmfield(paramsSim,'U');

paramsSim.u{1} = center(params.U{1});
paramsSim.u{2} = center(params.U{2});
paramsSim.u{3} = center(params.U{3});

% obtain random simulation results
simRes1 = cell(10,1);

for i = 1:length(simRes1)
    
    % set initial state, input
    if i == 1
        paramsSim.x0 = Zcenter + Zdelta;
    elseif i == 2
        paramsSim.x0 = Zcenter - Zdelta;
    else
        paramsSim.x0 = randPoint(zonotope(Zcenter,Zdelta));
    end 
    
    %simulate hybrid automaton
    HAsim = simulate(HA,paramsSim); 
    simRes1{i} = get(HAsim,'trajectory');
end




% Reachability Analysis 1 -------------------------------------------------

tStart = tic;

params.R0 = R01;
HA_11 = reach(HA,params,options);

params.R0 = R02;
HA_12 = reach(HA,params,options);

params.R0 = R03;
HA_13 = reach(HA,params,options);

params.R0 = R04;
HA_14 = reach(HA,params,options);

tComp1 = toc(tStart);



% Parameter ---------------------------------------------------------------

% update initial set
Rtemp = get(HA_11,'reachableSet');
R01 = Rtemp{end}.T{end};

Rtemp = get(HA_12,'reachableSet');
R02 = Rtemp{end}.T{end};

Rtemp = get(HA_13,'reachableSet');
R03 = Rtemp{end}.T{end};

Rtemp = get(HA_14,'reachableSet');
R04 = Rtemp{end}.T{end};

% update start and final time
params.tStart = 0.2;
params.tFinal = 2;

% update system input
u = [5; 0];             % acceleration and load torque

params.U{1} = B1*zonotope(0) + B1*u + c1;
params.U{2} = B2*zonotope(0) + B2*u + c2; 
params.U{3} = B3*zonotope(0) + B3*u + c3;



% Simulation 2 ------------------------------------------------------------

% simulation parameter
paramsSim = params;
paramsSim = rmfield(paramsSim,'U');
paramsSim = rmfield(paramsSim,'R0');

paramsSim.u{1} = center(params.U{1});
paramsSim.u{2} = center(params.U{2});
paramsSim.u{3} = center(params.U{3});

% obtain random simulation results
simRes2 = cell(length(simRes1),1);

for i = 1:length(simRes1)
    
    % set initial state
    paramsSim.x0 = simRes1{i}.x{1}(end,:); 
    
    % simulate hybrid automaton
    HAsim = simulate(HA,paramsSim); 
    simRes2{i} = get(HAsim,'trajectory');
end



% Reachability Analysis 2 -------------------------------------------------

tStart = tic;

params.R0 = R01;
HA_21 = reach(HA,params,options);

params.R0 = R02;
HA_22 = reach(HA,params,options);

params.R0 = R03;
HA_23 = reach(HA,params,options);

params.R0 = R04;
HA_24 = reach(HA,params,options);

tComp2 = toc(tStart);

disp(['computation time: ',num2str(tComp1 + tComp2)]);



% Visualization -----------------------------------------------------------

figure; hold on

% plot reachable set
plotReachFilled(HA_11,[1,3],[.6 .6 .6]);
plotReachFilled(HA_12,[1,3],[.6 .6 .6]);
plotReachFilled(HA_13,[1,3],[.6 .6 .6]);
plotReachFilled(HA_14,[1,3],[.6 .6 .6]);

plotReachFilled(HA_21,[1,3],[.6 .6 .6]);
plotReachFilled(HA_22,[1,3],[.6 .6 .6]);
plotReachFilled(HA_23,[1,3],[.6 .6 .6]);
plotReachFilled(HA_24,[1,3],[.6 .6 .6]);

% plot simulation
for i = 1:length(simRes1)
    for j = 1:length(simRes1{i}.x)
        plot(simRes1{i}.x{j}(:,1),simRes1{i}.x{j}(:,3),'k');
    end
end
for i = 1:length(simRes2)
    for j = 1:length(simRes2{i}.x)
        plot(simRes2{i}.x{j}(:,1),simRes2{i}.x{j}(:,3),'k');
    end
end

% guard set
plot([0.03 0.03],[-10 90],'r');
plot([-0.03 -0.03],[-10 90],'r');

% formatting
xlabel('x_1');
ylabel('x_3');
xlim([-0.1,0.2]);
ylim([-10,90]);
box on


% example completed
completed = 1;

%------------- END OF CODE --------------