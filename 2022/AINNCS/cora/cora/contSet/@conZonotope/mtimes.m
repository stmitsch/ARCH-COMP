function cZ = mtimes(factor1,factor2)
% mtimes - Overloaded '*' operator for the multiplication of a matrix or
%    an interval matrix with a constrained zonotope
%
% Syntax:  
%    cZ = mtimes(factor1,factor2)
%
% Inputs:
%    factor1 - conZonotope object, numerical or interval matrix
%    factor2 - conZonotope object, numerical or interval matrix 
%
% Outputs:
%    cZ - conZonotope object
%
% Example: 
%    Z = [0 1 0 1;0 1 2 -1];
%    A = [-2 1 -1]; b = 2;
%    cZ = conZonotope(Z,A,b);
%    res = [3 1;2 4] * cZ;
%
%    figure; hold on;
%    plot(cZ,[1,2],'r');
%    plot(res,[1,2],'b');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Niklas Kochdumper
% Written:      15-May-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% Call superclass method
if ~isnumeric(factor1)
    throw(CORAerror('CORA:noops',factor1,factor2));
else
    cZ = factor2;
    cZ.Z = factor1*factor2.Z;
end

%------------- END OF CODE --------------