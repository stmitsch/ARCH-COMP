FROM ubuntu:18.04

# Install requirements
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install curl gnuplot less libfontconfig1 libgomp1 libwww-perl mercurial rlwrap unzip

# Install isabelle
RUN hg clone http://isabelle.in.tum.de/repos/isabelle -r 12f455cc6573
RUN ./isabelle/bin/isabelle components -I
RUN ./isabelle/bin/isabelle components -a
RUN mkdir -p /root/.isabelle/etc

# install AFP
RUN hg clone https://bitbucket.org/isa-afp/afp-devel -r d798380a6ca7

# update for ARCH-COMP 2020
COPY arch2020.patch /
RUN cd afp-devel && hg patch --no-commit ../arch2020.patch
RUN mkdir -p /result

# build base image
RUN echo ML_system_64 = "true" > /root/.isabelle/etc/preferences
RUN echo 'ML_OPTIONS="--minheap 4000 --maxheap 4000"' >> /root/.isabelle/etc/settings
RUN ./isabelle/bin/isabelle jedit -b
RUN ./isabelle/bin/isabelle build -b Pure
RUN ./isabelle/bin/isabelle build -b HOL
RUN ./isabelle/bin/isabelle build -b HOL-Analysis
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b Ordinary_Differential_Equations
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-Numerics

RUN chmod u+x afp-devel/thys/Ordinary_Differential_Equations/Ex/ARCH_COMP/plot_arch_comp

CMD ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-ARCH-COMP && \
  cd afp-devel/thys/Ordinary_Differential_Equations/Ex/ARCH_COMP && \
  ./plot_arch_comp && \
  cp *.pdf /result && \
  cp results.csv /result
