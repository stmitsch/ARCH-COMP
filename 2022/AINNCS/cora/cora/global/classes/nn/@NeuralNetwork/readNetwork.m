function obj = readNetwork(file_path)
% readNetwork - reads and converts a network according to file ending
%
% Syntax:
%    res = NeuralNetwork.readNetwork(file_path)
%
% Inputs:
%    file_path: path to file
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralnetwork2cora

% Author:       Tobias Ladner
% Written:      30-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

[~, ~, type] = fileparts(file_path);

if strcmp(type, '.onnx')
    obj = NeuralNetwork.readONNXNetwork(file_path);
elseif strcmp(type, '.nnet')
    obj = NeuralNetwork.readNNetNetwork(file_path);
elseif strcmp(type, '.yml')
    obj = NeuralNetwork.readYMLNetwork(file_path);
elseif strcmp(type, '.sherlock')
    obj = NeuralNetwork.readSherlockNetwork(file_path);
else
    error(sprintf('NN models of this type are not supported: "%s"', type));
end

end