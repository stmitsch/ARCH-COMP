function [res,points] = convexHullAlg(obj, R, params, options)
    % convexHullAlg - simulates a system using the convex hull of the
    % trajectories
%
% Syntax:
%    res = convexHullAlg(obj, R, params, options)
%    [res,points] = convexHullAlg(obj, R, params, options)
%
% Inputs:
%    obj - contDynamics object
%    R - object of class reachSet storing the computed reachable set
%    params - struct containing the parameter that define the 
%             reachability problem
%    options - struct containing settings for the random simulation
%
%       .points:    number of random initial points (positive integer)
%       .vertSamp:  flag that specifies if random initial points, inputs,
%                   and parameters are sampled from the vertices of the 
%                   corresponding sets (0 or 1)
%       .strechFac: stretching factor for enlarging the reachable sets 
%                   during execution of the algorithm (scalar > 1).
%       last two are only necessary due to validateOptions
%
% Outputs:
%    res - object of class simResult storing time and states of the
%          simulated trajectories
%    points - final points of the simulation
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Fabian Frank
% Written:      2-August-2021
% Last update:  2-August-2021
% Last revision:---

%------------- BEGIN CODE --------------

% new options preprocessing
%requires own optionCheck
options = validateOptions(obj,'simulateRRT',params,options);

% options preprocessing
%options = params2options(params,options);
%options = checkOptionsSimulate(obj,options,false);

% set simulation options
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
% generate overall options
opt = odeset(stepsizeOptions);

% initialize
X_sample_size = 2*rad(interval(R.timePoint.set{1}));
normMatrix = diag(1./X_sample_size);

% obtain set of uncertain inputs 
if isfield(options,'uTransVec')
    U = options.uTransVec(:,1) + options.U;
else
    U = options.uTrans + options.U;
end

% possible extreme inputs
V_input = vertices(U);
V_input_mat = V_input;
nrOfExtrInputs = length(V_input_mat(1,:));
%nrOfExtrInputs

% initialize simulation results
x = cell(length(R.timePoint.set)*options.points,1);
t = cell(length(R.timePoint.set)*options.points,1);
cnt = 1;

%init start set of vertices
V_startObject = vertices(options.R0);
% init obtained states from the RRT


% loop over all time steps
for iStep = 1:length(R.timePoint.set)
%for iStep = 1:2

    
    iStep
    
    % update time
    options.tStart = infimum(R.timeInterval.time{iStep});
    options.tFinal = supremum(R.timeInterval.time{iStep});
    V_startObjectN = V_startObject;
    % loop over all trajectories
    for iSample = 1:length(V_startObject) 
    
        %select each trajetory once
        options.x0 = V_startObject(:,iSample);

        
        % update set of uncertain inputs when tracking
        if isfield(options,'uTransVec')
            U = options.uTransVec(:,iStep) + options.U;
            V_input = vertices(U);
            V_input_mat = V_input;
        end

        %simulate model for every vertex of the input
        for iInput = 1:nrOfExtrInputs
            %set input
            options.u = V_input_mat(:,iInput);
            %simulate
            [t_traj{iInput},x_traj{iInput}] = simulate(obj,options,opt);   
            x_next(:,iInput) = x_traj{iInput}(end,:);  
            x{cnt} = x_traj{iInput};
            t{cnt} = t_traj{iInput};
            cnt = cnt + 1;
        end
        
        V_startObjectN = cat(2,V_startObjectN,x_next);
    end
    %update set of trajectories
    %compute convex hull and then choose the vertices
    V_startObjectN = mptPolytope.enclosePoints(V_startObjectN);
    V_startObjectN = vertices(V_startObjectN);
    V_startObject = V_startObjectN;
    %length(V_startObjectN)
    

end

% construct object storing the simulation results
res = simResult(x,t);
